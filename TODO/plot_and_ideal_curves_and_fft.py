workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step2/"

import pandas
import numpy
import os
import re
import math
import matplotlib.pyplot as plt

filenames=[]
for (dirname,_,files) in os.walk(workdir):
    for f in files:
        if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None:
        #if re.search("^030\.csv$",f) != None:
            filenames=filenames+[dirname+"/"+f]
filenames.sort()

rate=100.0
def ideal_arc_curve():
    arc=[40*math.sin(i*2.0/rate*math.pi+math.pi/2) for i in range(12000)]
    return arc
        
def ideal_force_curve1():
    def f(i):
        if i%round(rate)>rate/2.0:
            return 200
        else:
            return 0
    
    force=[f(i) for i in range(12000)]
    return force
        
co=0.5
def ideal_force_curve2():
    def f(i):
        if i%round(rate)>rate/2.0:
            co_new=0.1+0.9*math.sin(-i*2.0/rate*math.pi)
            return 200*(co_new+(1-co_new)*math.sin(-i*2.0/rate*math.pi))
        else:
            return 0
    
    force=[f(i) for i in range(12000)]
    return force
   
'''
plt.figure(1)
plt.plot(ideal_force_curve1(),color="blue")
plt.plot(ideal_arc_curve(),color="green")
plt.show()
plt.figure(1)
fft1=numpy.fft.fft(ideal_force_curve1()[0:1000])[0:1000]
plt.plot(fft1,color="blue")
plt.show()
'''

plt.figure(1)
plt.plot(ideal_force_curve2(),color="blue")
plt.plot(ideal_arc_curve(),color="green")
plt.show()
plt.figure(1)
fft1=numpy.fft.fft(ideal_force_curve2()[0:10000])[0:1000]
plt.plot(fft1,color="blue")
plt.show()

for f in filenames:
    df = pandas.read_csv(f,sep="\t")
    if len(df.Name1)>10000:
        print f
        print df.Name1[0]
        print str((df.Force1-df.Force2).mean())
        plt.figure(1)
        plt.plot(df.Force1,color="blue")
        plt.plot(df.Arc1,color="green")
        plt.plot(df.Force2,color="red")
        plt.show()
        plt.figure(1)
        fft1=numpy.fft.fft(df.Force1[0:10000])[10:100]
        fft2=numpy.fft.fft(df.Force2[0:10000])[10:100]
        plt.plot(fft1,color="blue")
        plt.plot(fft2,color="red")
        plt.show()
