workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step5/"
outdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step6/"

effective_force_limit=20 # N
attributes=["Arc1","Arc2","Force1","Force2","Velocity","AccX","AccZ"]
prefixes=["","Catch","Release"]

import pandas
import numpy
import math
import matplotlib.pyplot as plt
import json
import random
import os
import re
from sklearn.decomposition import PCA
import sklearn.preprocessing
import io
import functools
from sklearn.cluster import DBSCAN
from scipy.fftpack import rfft, irfft, fftfreq

def write_log(text,to_screen_too=False,suffix="\n",severity="INFO"):
    write_log.wr.write("["+severity+"]"+text+suffix)
    if to_screen_too:
        print text
write_log.wr=io.FileIO(outdir+"extract_features.log",mode="w")

# filtering an array, averaging the 2 neighbour and the value, for all value
def filter_array(array):
    result=array.copy()
    result[0]=(array[0]+array[1])/2.0
    for i in range(1,len(array)-1):
        result[i]=sum(array[i-1:i+2])/3.0
    result[len(array)-1]=(array[len(array)-2]+array[len(array)-1])/2.0
    return result

def get_drive_time(phase_codes,time_resolution):
    count=0
    for phase in phase_codes:
        if phase == 'd':
            count=count+1
    return count*time_resolution

def get_effective_arc(arc,force):
    force_fil=filter_array(force)
    arc_fil=filter_array(arc)
    i = 0
    while i < len(arc_fil) and force_fil[i]<effective_force_limit:
        i=i+1
    if i < len(arc_fil):
        min_arc=arc_fil[i]
    else:
        min_arc=arc.min()
        max_arc=arc.max()
    while i < len(arc_fil) and force_fil[i]>effective_force_limit:
        i=i+1
    if i < len(arc_fil):
        max_arc=arc_fil[i]
    else:
        max_arc=arc.max()
    if max_arc<arc.max()-15:
        max_arc=arc.max()
        write_log("WARNING: couldn't reliable calc, effective arc!")
    if min_arc>arc.min()+15:
        min_arc=arc.min()
        write_log("WARNING: couldn't reliable calc, effective arc!")
    return max_arc-min_arc

# Returns the list of columns with the given prefix
def columns_with_prefix(df,prefix=""):
    result=[]
    for i in range(len(df.columns)):
        if re.search("^"+prefix,df.columns[i]) != None:
            result=result+[df.columns[i]]
    return result

# Computes mean squared error of the curves
def msqe(c1,c2):
    result=0
    for i in range(len(c1)):
        result=result+(c1[i]-c2[i])*(c1[i]-c2[i])
    return result

# Offsets the curve's minimum value to zero
def offset_to_zero(curve):
    min_c=curve.min()
    o_curve=curve.copy()
    for i in range(len(curve)):
        o_curve[i]=curve[i]-min_c
    return o_curve
        
# Normalizes the curve area to 1
def normalize_area(curve):
    count=len(curve)
    area=0
    for point in curve:
        area=area+point/count
    n_curve=curve.copy()
    if area==0:
        write_log("Area under curve is zero- possibly constant zero data.",severity="WARNING")
        return n_curve
    for i in range(count):
        n_curve[i]=curve[i]/area
    return n_curve

# Converts the polygon given with its corner points to curve
def corner_points_to_curve(points):
    length=points[len(points)-1][0]-points[0][0]+1
    curve=[0 for i_ in range(length)]
    for i in range(len(points)-1):
        for x in range(points[i][0],points[i+1][0]):
            curve[x]=points[i][1]+(points[i+1][1]-points[i][1])*float(x-points[i][0])/float(points[i+1][0]-points[i][0])
    curve[points[len(points)-1][0]]=points[len(points)-1][1]
    return curve

# Returns the slope of the curve-segment, represented by the corner-points, startindex is the index of the first corner-point
def slope(points,start_index,curve_length):
    if points[start_index+1][0]-points[start_index][0]!=0:
        return (points[start_index+1][1]-points[start_index][1])/(points[start_index+1][0]-points[start_index][0])*curve_length
    else:
        return (points[start_index+1][1]-points[start_index][1])*curve_length

def binary_search(error_func,param_init_value,init_step_size,exit_treshold=0.02,step_refinment=0.25):
    if init_step_size < param_init_value * exit_treshold:
        return param_init_value
    write_log("Binary search with params: "+str(param_init_value)+","+str(init_step_size),severity="DEBUG")
    param=param_init_value
    step_size=init_step_size
    mid=error_func(param)
    high=error_func(param+step_size)
    low=error_func(param-step_size)
    if high<mid and param+step_size<=1.0:
        if mid-high<exit_treshold:
            return param
        else:
            return binary_search(error_func,param+step_size,step_size)
    elif low<mid and param-step_size>=0.0:
        if mid-low<exit_treshold:
            return param
        else:
            return binary_search(error_func,param-step_size,step_size)
    else: 
        return binary_search(error_func,param,step_size*step_refinment)



# Fit a line-trapez-line combination to the force curve
#   returns features of the lines (horiz. length+2 slope value)
def fit_force(force):
    # Fit a line-trapez-line combination to the (normalized!!) force curve, with the given boundary params
    #   returns the params: (first point-height, line pitch, ... - for all 5 lines)
    def fit_force_for_treshold(n_force,low_b,high_b):
        points=[[0,0] for i_ in range(6)]
        max_f=n_force.max()
        #min_f=0
        try:
            i=0
            points[0][0]=0
            points[0][1]=max_f*low_b
            while n_force[i]<max_f*low_b:
                i=i+1
            points[1][0]=i
            points[1][1]=max_f*low_b
            while n_force[i]<max_f*high_b:
                i=i+1
            points[2][0]=i
            points[2][1]=max_f*high_b
            while n_force[i]>max_f*high_b:
                i=i+1
            points[3][0]=i
            points[3][1]=max_f*high_b
            while n_force[i]>max_f*low_b:
                i=i+1
            points[4][0]=i
            points[4][1]=max_f*low_b    
            points[5][0]=len(n_force)-1
            points[5][1]=max_f*low_b  
        except IndexError:
            points=[[0,i_] for i_ in range(6)]
            points[5][0]=len(n_force)-1
        return points
    
    def msqe_from_boundaries(n_force,low_b,high_b):
        return msqe(corner_points_to_curve(fit_force_for_treshold(n_force,low_b,high_b)),n_force)
    
    def feature_vector_from_6points(points):
        w=[float(points[i_+1][0]-points[i_][0])/len(force) for i_ in range(5)]
        slope2=slope(points,1,len(force))
        slope4=slope(points,3,len(force))
        return w+[slope2,slope4]
    
    n_force=normalize_area(offset_to_zero(force))
    high_b=0.95
    p1=functools.partial(msqe_from_boundaries,n_force,high_b=high_b)
    low_b=binary_search(p1,0.02,0.018)
    p2=functools.partial(msqe_from_boundaries,n_force,low_b)
    write_log("Low_b is:"+str(low_b),severity="DEBUG")
    high_b=binary_search(p2,high_b,0.04)
    write_log("High_b is:"+str(high_b),severity="DEBUG")
    fitted=fit_force_for_treshold(n_force,low_b,high_b)
    msqe_=msqe(n_force,corner_points_to_curve(fitted))
    write_log("MSQE is:"+str(msqe_),severity="DEBUG")
    return feature_vector_from_6points(fitted)+[msqe_]
    '''
    plt.figure(1)
    plt.plot(corner_points_to_curve(fitted),c="red")
    plt.plot(n_force,c="blue")
    plt.show()
    '''
    
# Extract features from the velocity curve
def velocity_features(velocity):
    # Fits the points of the line-triangle-line shape with the given x1,x2 values
    def fit_vel_for_treshold(n_vel,x1,x2):
        if x1 <= 0:
            x1=0.0
        elif x1>=1:
            x1=1.0
        if x2<=0:
            x2=0.0
        elif x2>=1:
            x2=1.0
            
        min_vel=min(n_vel)
        points=[[0,0] for i_ in range(5)]
        points[0][0]=0
        points[1][0]=int(round(len(n_vel)*x1))-1
        i=0
        while n_vel[i]>min_vel:
            i=i+1
        points[2][0]=i
        points[3][0]=int(round(len(n_vel)*x2))-1
        points[4][0]=len(n_vel)-1
        
        points[0][1]=n_vel[0]
        points[1][1]=n_vel[0]
        points[2][1]=min_vel
        points[3][1]=n_vel[points[3][0]]
        points[4][1]=n_vel[len(n_vel)-1]
    
        return points
    
    def msqe_from_params(n_vel,x1,x2):
        return  msqe(corner_points_to_curve(fit_vel_for_treshold(n_vel,x1,x2)),n_vel)
    
    # Fits a (horiz).line - triangle-line combination to the velocity curve
    def fit_vel(n_vel):
        x1=0.3
        x2=0.8
        p1=functools.partial(msqe_from_params,n_vel,x2=x2)
        x1=binary_search(p1,x1,0.26)
        p2=functools.partial(msqe_from_params,n_vel,x1)
        x2=binary_search(p2,x1+0.3,0.27)
        points=fit_vel_for_treshold(n_vel,x1,x2)
        
        '''
        fitted=fit_vel_for_treshold(n_vel,x1,x2)
        plt.figure(1)
        plt.plot(corner_points_to_curve(fitted),c="red")
        plt.plot(n_vel,c="blue")
        plt.plot(velocity,c="green")
        plt.show()
        '''
        return points
        
    def extract_curve_features(vel_points):
        w=[float(vel_points[i_+1][0]-vel_points[i_][0])/len(velocity) for i_ in range(4)]
        slope2=slope(vel_points,1,len(velocity))
        slope3=slope(vel_points,2,len(velocity))
        slope4=slope(vel_points,3,len(velocity))
        return w+[slope2,slope3,slope4]
    
    n_vel=normalize_area(offset_to_zero(velocity))
    n_vel_diff=n_vel[:-1]-n_vel[1:]
    velocity_smoothness=numpy.std(n_vel_diff)
    fitted=fit_vel(n_vel)
    curve_features=extract_curve_features(fitted)
    return [velocity_smoothness]+curve_features+[msqe(corner_points_to_curve(fitted),n_vel)]

# Extract arc curve features
def arc_features(arc):
    def fit_arc_for_treshold(n_arc,x1,x2):
        if x1 <= 0:
            x1=0.0
        elif x1>=1:
            x1=1.0
        if x2<=0:
            x2=0.0
        elif x2>=1:
            x2=1.0
            
        max_arc=max(n_arc)
        points=[[0,0] for i_ in range(5)]
        points[0][0]=0
        points[1][0]=int(round(len(n_arc)*x1))-1
        i=0
        while n_arc[i]<max_arc:
            i=i+1
        points[2][0]=i
        points[3][0]=int(round(len(n_arc)*x2))-1
        points[4][0]=len(n_arc)-1
        
        points[0][1]=n_arc[0]
        points[1][1]=n_arc[0]
        points[2][1]=max_arc
        points[3][1]=n_arc[0]
        points[4][1]=n_arc[0]
    
        return points
    
    def msqe_from_params(n_arc,x1,x2):
        return  msqe(corner_points_to_curve(fit_arc_for_treshold(n_arc,x1,x2)),n_arc)
    
    # Fits a (horiz).line - triangle-(horiz) line combination to the arc curve
    def fit_arc(n_arc):
        x1=0.1
        x2=0.9
        p1=functools.partial(msqe_from_params,n_arc,x2=x2)
        x1=binary_search(p1,x1,0.095)
        p2=functools.partial(msqe_from_params,n_arc,x1)
        x2=binary_search(p2,x2,0.095)
        points=fit_arc_for_treshold(n_arc,x1,x2)
        '''
        if random.random()<-0.1:
            plt.figure(1)
            plt.title="Arc"
            plt.plot(corner_points_to_curve(points),c="red")
            plt.plot(n_arc,c="blue")
            #plt.plot(arc,c="green")
            plt.show()
        '''

        return points

    def extract_curve_features(arc_points):
        w=[float(arc_points[i_+1][0]-arc_points[i_][0])/len(arc) for i_ in range(4)]
        slope2=slope(arc_points,1,len(arc))
        slope3=slope(arc_points,2,len(arc))
        return w+[slope2,slope3]

    n_arc=normalize_area(offset_to_zero(arc))
    fitted=fit_arc(n_arc)
    return extract_curve_features(fitted)+[msqe(corner_points_to_curve(fitted),n_arc)]

# Extract curve shape features from catch or release phase arcs
def catch_arc_features(arc_catch):    
    def fit_arc_for_treshold(n_arc,x1,x2):
        if x1 <= 0:
            x1=0.0
        elif x1>=1:
            x1=1.0
        if x2<=0:
            x2=0.0
        elif x2>=1:
            x2=1.0
            
        points=[[0,0] for i_ in range(4)]
        points[0][0]=0
        points[1][0]=int(round(len(n_arc)*x1))-1
        points[2][0]=int(round(len(n_arc)*x2))-1
        points[3][0]=len(n_arc)-1
        
        points[0][1]=n_arc[0]
        points[1][1]=n_arc[int(round(len(n_arc)*0.5))-1]
        points[2][1]=points[1][1]
        points[3][1]=n_arc[points[3][0]]
    
        return points
    
    def msqe_from_params(n_arc,x1,x2):
        return  msqe(corner_points_to_curve(fit_arc_for_treshold(n_arc,x1,x2)),n_arc)
    
    # Fits a (horiz).line - triangle-(horiz) line combination to the arc curve
    def fit_arc(n_arc):
        x1=0.3
        x2=0.7
        p1=functools.partial(msqe_from_params,n_arc,x2=x2)
        x1=binary_search(p1,x1,0.25)
        p2=functools.partial(msqe_from_params,n_arc,x1)
        x2=binary_search(p2,x1+0.4,0.25)
        if x2<x1:
            x2=x1
        points=fit_arc_for_treshold(n_arc,x1,x2)
        '''
        if random.random()<-0.1:
            plt.figure(1)
            plt.title="Catch"
            plt.plot(corner_points_to_curve(points),c="red")
            plt.plot(n_arc,c="blue")
            plt.plot(normalize_area(offset_to_zero(arc_catch)),c="m")
            #plt.plot(arc_catch,c="green")
            plt.show()
        '''

        return points

    def extract_curve_features(arc_points):
        w=[float(arc_points[i_+1][0]-arc_points[i_][0])/len(arc_catch) for i_ in range(3)]
        slope1=slope(arc_points,0,len(arc_catch))
        slope3=slope(arc_points,2,len(arc_catch))
        return w+[slope1,slope3]

    n_arc=normalize_area(offset_to_zero(arc_catch))
    fitted=fit_arc(n_arc)
    return extract_curve_features(fitted)+[msqe(corner_points_to_curve(fitted),n_arc)]

# The deegre when force reaches the given % of max force
def force_up_to_percent(force,p,arc):
    treshold=max(force)*p
    n=len(force)
    i=0
    while force[i] < treshold and i < n:
        i=i+1
    if i < n:
        return arc[i]-min(arc)
    write_log("The force didn't reach it "+str(p*100)+"%value.",to_screen_too=True,severity="ERROR")
    return None
    
# The deegre when force declines the given % of max force
def force_down_from_percent(force,p,arc):
    treshold=max(force)*p
    n=len(force)
    i=n-1
    while force[i] < treshold and i >= 0:
        i=i-1
    if i>=0:
        return arc[i]-min(arc)
    write_log("The force didn't reach it "+str(p*100)+"%value.",to_screen_too=True,severity="ERROR")
    return None
    
# Returns that part of the (force) curve, that is greater than a given treshold
def greater_than_subsequence(curve,treshold):
    n=len(curve)
    beginning=0
    while curve[beginning] < treshold and beginning < n:
        beginning=beginning+1
    if beginning >= n:
        write_log("The curve didn't reach the given treshold ("+str(treshold)+").",to_screen_too=True,severity="ERROR")
        return None
    end=n-1
    while curve[end] < treshold and end >= 0:
        end=end-1
    if end < 0 or beginning>end:
        write_log("The curve didn't reach the given treshold ("+str(treshold)+") or some algorithmic problem.",to_screen_too=True,severity="ERROR")
        return None
    if beginning>0:
        beginning=beginning-1
    if end<n-1:
        end=end+1
    return curve[beginning:end+1]

# Returns the average value of the parts of the (force) curve
def split_to_parts(curve,n_parts,normalize=True):
    if normalize:
        curve=greater_than_subsequence(curve,min(100,max(curve)*0.3))
        curve=normalize_area(offset_to_zero(curve))
    per_part=float(len(curve))/n_parts
    parts=[0 for i in range(n_parts)]
    for i in range(n_parts-1):
        i1=int(math.floor(i*per_part))
        i2=int(math.floor((i+1)*per_part))
        if i2!=i1:
            parts[i]=float(sum(curve[i1:i2]))/(i2-i1)
        elif i!=0:
            parts[i]=parts[i-1]
        else:
            parts[i]=0.0
    i1=int(math.floor((n_parts-1)*per_part))
    i2=len(curve)
    if i2!=i1:
        parts[n_parts-1]=float(sum(curve[i1:i2]))/(i2-i1)
    else:
        parts[n_parts-1]= parts[n_parts-2]
    
    if random.random()<-0.05:
        plt.figure(1)
        plt.plot(curve,color="red")
        for i in range(len(parts)):
            plt.hlines(parts[i],i*per_part,(i+1)*per_part,color="blue")
        plt.show()
    
    return parts

# Returns the differences beetween the parts of the (force) curve
def diff_beetwen_parts(curve,n_parts):
    curve=greater_than_subsequence(curve,min(100,max(curve)*0.3))
    curve=normalize_area(offset_to_zero(curve))
    parts=split_to_parts(curve,n_parts,normalize=False)
    diffs=[parts[i+1]-parts[i] for i in range(n_parts-1)]
    
    if random.random()<-0.05:
        per_part=float(len(curve))/n_parts
        plt.figure(1)
        plt.plot(curve,color="red")
        for i in range(len(diffs)):
            i1=int(math.floor(i*per_part))
            i2=int(math.floor((i+1)*per_part))
            plt.vlines(i2,0,max(curve),color="black",alpha=0.4)
            plt.vlines(i2,parts[i],parts[i]+diffs[i],color="blue")
        plt.vlines(len(curve),0,max(curve),color="black",alpha=0.4)
        print len(curve)
        plt.show()
    
    return diffs
    
# Returns the autocorrelation of the given (same length) curves
def autocorrelation(curve1,curve2):
    ac=0
    for i in range(len(curve1)):
        ac=ac+curve1[i]*curve2[i]
    return float(ac)/numpy.std(curve1)/numpy.std(curve2)

# Returns force stability features based on to consecutive force
def force_stability_features(force1,force2):
    if force1 is None or force2 is None:
        write_log("Cannot calculate force stability features, no previos or next force.",severity="DEBUG")
        return [None,None]
    
    force1=split_to_parts(force1,50)
    force2=split_to_parts(force2,50)
    return [msqe(force1,force2),autocorrelation(force1,force2)]
        
# Returns true, if the curve is a constant line, az zero
def is_zero_curve(curve):
    for i_ in range(len(curve)):
        if curve[i_]!=0.0:
            return False
    return True

# List files
filenames=[]
for (dirname,_,files) in os.walk(workdir):
    for f in files:
        if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None:
            filenames=filenames+[dirname+"/"+f]
filenames.sort()

rows=[]
for f in filenames:
    write_log("Processing "+f,to_screen_too=True)
    df=pandas.read_csv(f,sep="\t")
    #if df.Name1[0]!=numpy.int64(0):
    #    continue
    #if df.MeasurementID[0]!=numpy.int64(35):
    #    continue
    
    # In column names CL prefix stand for 'can be used in clustering', ME is 'cannot, it is only additional meta-data'
    # The naming convenzions are important: CL for ordinal rows, ME for metadata, MEOR for original data
    #  columns with Name1,Arc1,Length1,Force1 and _1 keywords (and not containing '12') are considered to belongs to the side 1 (for 2 is the same) 
    #  columns without these numbers are considered the belongs to both sides
    columns=["CLFullArc1","CLFullArc2","CLAvgArc12Diff","CLFullLength_1","CLFullLength_2","CLEffectiveArc1","CLEffectiveArc2"]
    columns=columns+["CLEffectiveLength1","CLEffectiveLength2","CLAverageHandleSpeed","CLAverageForce12Diff"]
    columns=columns+["CLDrivePerRecoveryTime"]
    columns=columns+["CLForce1W1","CLForce1W2","CLForce1W3","CLForce1W4","CLForce1W5","CLForce1Slope2","CLForce1Slope4","CLForce1TrapezMSQE"]
    columns=columns+["CLForce2W1","CLForce2W2","CLForce2W3","CLForce2W4","CLForce2W5","CLForce2Slope2","CLForce2Slope4","CLForce2TrapezMSQE"]
    columns=columns+["CLVelocitySmoothness","CLVelocityW1","CLVelocityW2","CLVelocityW3","CLVelocityW4","CLVelocitySlope2","CLVelocitySlope3","CLVelocitySlope4","CLVelocityTriangleMSQE"]
    columns=columns+["CLArc1W1","CLArc1W2","CLArc1W3","CLArc1W4","CLArc1Slope2","CLArc1Slope3","CLArc1TriangleMSQE"]
    columns=columns+["CLArc2W1","CLArc2W2","CLArc2W3","CLArc2W4","CLArc2Slope2","CLArc2Slope3","CLArc2TriangleMSQE"]
    columns=columns+["CLCatchArc1W1","CLCatchArc1W2","CLCatchArc1W3","CLCatchArc1Slope1","CLCatchArc1Slope3","CLCatchArc1TrapezMSQE"]
    columns=columns+["CLCatchArc2W1","CLCatchArc2W2","CLCatchArc2W3","CLCatchArc2Slope1","CLCatchArc2Slope3","CLCatchArc2TrapezMSQE"]
    columns=columns+["CLReleaseArc1W1","CLReleaseArc1W2","CLReleaseArc1W3","CLReleaseArc1Slope1","CLReleaseArc1Slope3","CLReleaseArc1TrapezMSQE"]
    columns=columns+["CLReleaseArc2W1","CLReleaseArc2W2","CLReleaseArc2W3","CLReleaseArc2Slope1","CLReleaseArc2Slope3","CLReleaseArc2TrapezMSQE"]
    columns=columns+["CLDegForce1UpTo70","CLDegForce2UpTo70","CLDegForce1DownFrom70","CLDegForce2DownFrom70"]
    columns=columns+["CLDegForce1UpTo40","CLDegForce2UpTo40","CLDegForce1DownFrom40","CLDegForce2DownFrom40"]
    columns=columns+["CLDegForce12UpTo70Diff","CLDegForce12DownFrom70Diff","CLDegForce12UpTo40Diff","CLDegForce12DownFrom40Diff","CLForce12CombinedDiff"]
    columns=columns+["CLAvgPerMaxForce1","CLAvgPerMaxForce2","CLPeakForce1AtLengthPercent","CLPeakForce2AtLengthPercent"]
    columns=columns+["CLForce1Part1","CLForce1Part2","CLForce1Part3","CLForce1Part4","CLForce1Part5"]
    columns=columns+["CLForce2Part1","CLForce2Part2","CLForce2Part3","CLForce2Part4","CLForce2Part5"]
    columns=columns+["CLForce1Diff1","CLForce1Diff2","CLForce1Diff3","CLForce1Diff4","CLForce1Diff5","CLForce1Diff6","CLForce1Diff7","CLForce1Diff8","CLForce1Diff9"]
    columns=columns+["CLForce2Diff1","CLForce2Diff2","CLForce2Diff3","CLForce2Diff4","CLForce2Diff5","CLForce2Diff6","CLForce2Diff7","CLForce2Diff8","CLForce2Diff9"]
    columns=columns+["CLForce1StabilityMSQE1","CLForce1StabilityAutocorrelation1"]
    columns=columns+["CLForce2StabilityMSQE1","CLForce2StabilityAutocorrelation1"]

    columns=columns+["MEDriveTime","MERecoveryTime","MEStrokeRate","MEAvgVelocity"]
    columns=columns+["MEMaxForce1","MEMaxForce2","MEAvgForce1","MEAvgForce2","MEMaxArc1","MEMaxArc2","MEMinArc1","MEMinArc2"]
    columns=columns+["MEMeasurementID","MESessionID","MEName1","MEName2","MEDate","METime"]
    columns=columns+["MEORArc1","MEORArc2","MEORForce1","MEORForce2","MEORVelocity","MEORAccX","MEORAccZ"]
    columns=columns+["MEORCatchArc1","MEORCatchArc2","MEORReleaseArc1","MEORReleaseArc2"]

    inboard=df.OarInboard[0]/1000.0
    previous_force1=None
    previous_force2=None
    previous_mID=None
    for (idx,data) in df.iterrows():
        phase_codes=numpy.array(json.loads(data[df.columns.get_loc("PhaseCodes")].replace("'",'"')))
        drive_time=get_drive_time(phase_codes,df.TimeResolutionMs[0])
        stroke_time=len(phase_codes)*df.TimeResolutionMs[0]
        recovery_time=stroke_time-drive_time
        
        row=[]
        arc1=numpy.array(json.loads(data[df.columns.get_loc("Arc1")]))
        arc2=numpy.array(json.loads(data[df.columns.get_loc("Arc2")]))
        row=row+[arc1.max()-arc1.min()]
        row=row+[arc2.max()-arc2.min()]
        row=row+[abs((arc2-arc1).mean())]
        row=row+[(arc1.max()-arc1.min())/180*math.pi*inboard]
        row=row+[(arc2.max()-arc2.min())/180*math.pi*inboard]
        force1=numpy.array(json.loads(data[df.columns.get_loc("Force1")]))
        force2=numpy.array(json.loads(data[df.columns.get_loc("Force2")]))
        effective_arc1=get_effective_arc(arc1,force1)
        effective_arc2=get_effective_arc(arc2,force2)
        row=row+[effective_arc1]
        row=row+[effective_arc2]
        row=row+[effective_arc1/180*math.pi*inboard]
        row=row+[effective_arc2/180*math.pi*inboard]
        row=row+[((arc1.max()-arc1.min())/180*math.pi*inboard+(arc2.max()-arc2.min())/180*math.pi*inboard)/drive_time*1000]
        row=row+[abs((force1-force2).mean())]
        row=row+[drive_time/recovery_time]
        velocity=numpy.array(json.loads(data[df.columns.get_loc("Velocity")]))
        if (is_zero_curve(velocity)):
            continue
        
        force1_trapez_fit=fit_force(force1)
        force2_trapez_fit=fit_force(force2)
        row=row+force1_trapez_fit
        row=row+force2_trapez_fit
        row=row+velocity_features(velocity)
        row=row+arc_features(arc1)
        row=row+arc_features(arc2)
        
        catch_arc1=numpy.array(json.loads(data[df.columns.get_loc("CatchArc1")]))
        catch_arc2=numpy.array(json.loads(data[df.columns.get_loc("CatchArc2")]))
        release_arc1=numpy.array(json.loads(data[df.columns.get_loc("ReleaseArc1")]))
        release_arc2=numpy.array(json.loads(data[df.columns.get_loc("ReleaseArc2")]))
        
        row=row+catch_arc_features(catch_arc1)
        row=row+catch_arc_features(catch_arc2)
        row=row+catch_arc_features(release_arc1)
        row=row+catch_arc_features(release_arc2)
        
        force_deg_tresholds=[]
        force_deg_tresholds=force_deg_tresholds+[force_up_to_percent(force1,0.7,arc1)]
        force_deg_tresholds=force_deg_tresholds+[force_up_to_percent(force2,0.7,arc2)]
        force_deg_tresholds=force_deg_tresholds+[force_down_from_percent(force1,0.7,arc1)]
        force_deg_tresholds=force_deg_tresholds+[force_down_from_percent(force2,0.7,arc2)]
        force_deg_tresholds=force_deg_tresholds+[force_up_to_percent(force1,0.4,arc1)]
        force_deg_tresholds=force_deg_tresholds+[force_up_to_percent(force2,0.4,arc2)]
        force_deg_tresholds=force_deg_tresholds+[force_down_from_percent(force1,0.4,arc1)]
        force_deg_tresholds=force_deg_tresholds+[force_down_from_percent(force2,0.4,arc2)]        
        row=row+force_deg_tresholds
        row=row+[force_deg_tresholds[0]-force_deg_tresholds[1],
                 force_deg_tresholds[2]-force_deg_tresholds[3],
                 force_deg_tresholds[4]-force_deg_tresholds[5],
                 force_deg_tresholds[6]-force_deg_tresholds[7]]
        force12_combined_diff=0
        force_combined_diff_weights=[1.0 for i_ in range(len(force1_trapez_fit)-1+len(force_deg_tresholds))]
        for i in range(len(force1_trapez_fit)-1):
            diff_=force1_trapez_fit[i]-force2_trapez_fit[i]
            force12_combined_diff=force12_combined_diff+diff_*diff_*force_combined_diff_weights[i]
        for i in range(len(force_deg_tresholds)/2):
            diff_=force_deg_tresholds[2*i]-force_deg_tresholds[2*i+1]
            force12_combined_diff=force12_combined_diff+diff_*diff_*force_combined_diff_weights[i]
        row=row+[force12_combined_diff]
        
        row=row+[force1.mean()/force1.max()]
        row=row+[force2.mean()/force2.max()]
        row=row+[(force_up_to_percent(force1,1.0,arc1))/(arc1.max()-arc1.min())]
        row=row+[(force_up_to_percent(force2,1.0,arc2))/(arc2.max()-arc2.min())]
        row=row+split_to_parts(force1,5)
        row=row+split_to_parts(force2,5)
        row=row+diff_beetwen_parts(force1,10)
        row=row+diff_beetwen_parts(force2,10)
        
        # Force stability features
        if previous_mID!=data["MeasurementID"]:
            previous_force1=None
            previous_force1=None
        
        force1_p_stability=force_stability_features(previous_force1,force1)
        force2_p_stability=force_stability_features(previous_force2,force2)
        row=row+force1_p_stability
        row=row+force2_p_stability
        
        previous_mID=data["MeasurementID"]
        previous_force1=force1
        previous_force2=force2
        
        # NOT FOR ANALYSIS
        row=row+[drive_time]
        row=row+[recovery_time]
        row=row+[60000/stroke_time]
        row=row+[velocity.mean()]
        row=row+[force1.max()]
        row=row+[force2.max()]
        row=row+[force1.mean()]
        row=row+[force2.mean()]
        row=row+[arc1.max()]
        row=row+[arc2.max()]
        row=row+[arc1.min()]
        row=row+[arc2.min()]
        row=row+[df.MeasurementID[0]]
        row=row+[df.SessionID[0]]
        row=row+[df.Name1[0]]
        row=row+[df.Name2[0]]
        row=row+[df.Date[0]]
        row=row+[data[df.columns.get_loc("Time")]]
        
        row=row+[data[df.columns.get_loc("Arc1")],data[df.columns.get_loc("Arc2")]]
        row=row+[data[df.columns.get_loc("Force1")],data[df.columns.get_loc("Force2")]]
        row=row+[data[df.columns.get_loc("Velocity")]]
        row=row+[data[df.columns.get_loc("AccX")],data[df.columns.get_loc("AccZ")]]
        row=row+[data[df.columns.get_loc("CatchArc1")],data[df.columns.get_loc("CatchArc2")]]
        row=row+[data[df.columns.get_loc("ReleaseArc1")],data[df.columns.get_loc("ReleaseArc2")]]
    
        rows=rows+[row]
    
        #break
    #break
    #if re.search("007.csv",f) != None:
        #break
#import sys
#sys.exit(0)
df=pandas.DataFrame(data=rows,columns=columns)
df["CLForce1StabilityMSQE2"]=df.apply(lambda x: None,axis=1)
df["CLForce1StabilityAutocorrelation2"]=df.apply(lambda x: None,axis=1)
df["CLForce2StabilityMSQE2"]=df.apply(lambda x: None,axis=1)
df["CLForce2StabilityAutocorrelation2"]=df.apply(lambda x: None,axis=1)
df["CLForce1StabilityMSQE2"][:-1]=df["CLForce1StabilityMSQE1"].loc[1:]
df["CLForce1StabilityAutocorrelation2"][:-1]=df["CLForce1StabilityAutocorrelation1"].loc[1:]
df["CLForce2StabilityMSQE2"][:-1]=df["CLForce2StabilityMSQE1"].loc[1:]
df["CLForce2StabilityAutocorrelation2"][:-1]=df["CLForce2StabilityAutocorrelation1"].loc[1:]
df.to_csv(outdir+"features.csv",sep="\t",header=True,index=False)

# Separates the attributes belonging to one side into a separate dataframe
def separate_sides(df,side):
    
    def belongs_to_other_side(col, other_side):
        return (re.search("Arc"+other_side,col)!=None or re.search("Force"+other_side,col)!=None or\
            re.search("Name"+other_side,col)!=None or re.search("_"+other_side,col)!=None or  \
            re.search("Length"+other_side,col)!=None) and \
            re.search("12",col)==None
    
    cols=[]
    if side=='1':
        other_side='2'
    else:
        other_side='1'
    for col in df.columns:
        if not belongs_to_other_side(col,other_side):
            cols=cols+[col]
    result=df[cols]
    rename_dict={}
    for col in result.columns:
        if re.search("12",col)==None:
            new_name=col.replace("Arc"+side,"Arc0").replace("Force"+side,"Force0").\
                    replace("Name"+side,"Name0").replace("_"+side,"_0").replace("Length"+side,"Length0")
            rename_dict[col]=new_name
    result["MESide"]=side
    return result.rename(columns=rename_dict)

df1=separate_sides(df,"1").copy()
df2=separate_sides(df,"2").copy()
df1_2=pandas.concat([df1,df2])
df1_2.to_csv(outdir+"features_only_one_side.csv",sep="\t",header=True,index=False)
    
write_log("Feature extraction suceeded!",to_screen_too=True,severity="INFO")
write_log.wr.close()   
