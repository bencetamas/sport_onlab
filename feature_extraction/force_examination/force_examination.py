workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step5/"

import pandas
import numpy
import math
import matplotlib.pyplot as plt
import json
import random
import os
import re
from sklearn.decomposition import PCA
import sklearn.preprocessing
import io
import functools
from sklearn.cluster import DBSCAN

# Taken from other python code:
#*********************************************************************************
#*********************************************************************************
#*********************************************************************************
# Just a dummy method
def write_log(g=1,to_screen=2,severity=3):
    return

# Computes mean squared error of the curves
def msqe(c1,c2):
    result=0
    for i in range(len(c1)):
        result=result+(c1[i]-c2[i])*(c1[i]-c2[i])
    return result

# Offsets the curve's minimum value to zero
def offset_to_zero(curve):
    min_c=curve.min()
    o_curve=curve.copy()
    for i in range(len(curve)):
        o_curve[i]=curve[i]-min_c
    return o_curve
        
# Normalizes the curve area to 1
def normalize_area(curve):
    count=len(curve)
    area=0
    for point in curve:
        area=area+point/count
    n_curve=curve.copy()
    if area==0:
        write_log("Area under curve is zero- possibly constant zero data.",severity="WARNING")
        return n_curve
    for i in range(count):
        n_curve[i]=curve[i]/area
    return n_curve

# Converts the polygon given with its corner points to curve
def corner_points_to_curve(points):
    length=points[len(points)-1][0]-points[0][0]+1
    curve=[0 for i_ in range(length)]
    for i in range(len(points)-1):
        for x in range(points[i][0],points[i+1][0]):
            curve[x]=points[i][1]+(points[i+1][1]-points[i][1])*float(x-points[i][0])/float(points[i+1][0]-points[i][0])
    curve[points[len(points)-1][0]]=points[len(points)-1][1]
    return curve

# Returns the slope of the curve-segment, represented by the corner-points, startindex is the index of the first corner-point
def slope(points,start_index,curve_length):
    if points[start_index+1][0]-points[start_index][0]!=0:
        return (points[start_index+1][1]-points[start_index][1])/(points[start_index+1][0]-points[start_index][0])*curve_length
    else:
        return (points[start_index+1][1]-points[start_index][1])*curve_length

def binary_search(error_func,param_init_value,init_step_size,exit_treshold=0.02,step_refinment=0.25):
    if init_step_size < param_init_value * exit_treshold:
        return param_init_value
    write_log("Binary search with params: "+str(param_init_value)+","+str(init_step_size),severity="DEBUG")
    param=param_init_value
    step_size=init_step_size
    mid=error_func(param)
    high=error_func(param+step_size)
    low=error_func(param-step_size)
    if high<mid and param+step_size<=1.0:
        if mid-high<exit_treshold:
            return param
        else:
            return binary_search(error_func,param+step_size,step_size)
    elif low<mid and param-step_size>=0.0:
        if mid-low<exit_treshold:
            return param
        else:
            return binary_search(error_func,param-step_size,step_size)
    else: 
        return binary_search(error_func,param,step_size*step_refinment)

# Fit a line-trapez-line combination to the force curve
#   returns features of the lines (horiz. length+2 slope value)
def fit_force(force):
    # Fit a line-trapez-line combination to the (normalized!!) force curve, with the given boundary params
    #   returns the params: (first point-height, line pitch, ... - for all 5 lines)
    def fit_force_for_treshold(n_force,low_b,high_b):
        points=[[0,0] for i_ in range(6)]
        max_f=n_force.max()
        #min_f=0
        try:
            i=0
            points[0][0]=0
            points[0][1]=max_f*low_b
            while n_force[i]<max_f*low_b:
                i=i+1
            points[1][0]=i
            points[1][1]=max_f*low_b
            while n_force[i]<max_f*high_b:
                i=i+1
            points[2][0]=i
            points[2][1]=max_f*high_b
            while n_force[i]>max_f*high_b:
                i=i+1
            points[3][0]=i
            points[3][1]=max_f*high_b
            while n_force[i]>max_f*low_b:
                i=i+1
            points[4][0]=i
            points[4][1]=max_f*low_b    
            points[5][0]=len(n_force)-1
            points[5][1]=max_f*low_b  
        except IndexError:
            points=[[0,i_] for i_ in range(6)]
            points[5][0]=len(n_force)-1
        return points
    
    def msqe_from_boundaries(n_force,low_b,high_b):
        return msqe(corner_points_to_curve(fit_force_for_treshold(n_force,low_b,high_b)),n_force)
    
    def feature_vector_from_6points(points):
        w=[float(points[i_+1][0]-points[i_][0])/len(force) for i_ in range(5)]
        slope2=slope(points,1,len(force))
        slope4=slope(points,3,len(force))
        return w+[slope2,slope4]
    
    n_force=normalize_area(offset_to_zero(force))
    high_b=0.95
    p1=functools.partial(msqe_from_boundaries,n_force,high_b=high_b)
    low_b=binary_search(p1,0.02,0.018)
    p2=functools.partial(msqe_from_boundaries,n_force,low_b)
    write_log("Low_b is:"+str(low_b),severity="DEBUG")
    high_b=binary_search(p2,high_b,0.04)
    write_log("High_b is:"+str(high_b),severity="DEBUG")
    fitted=fit_force_for_treshold(n_force,low_b,high_b)
    msqe_=msqe(n_force,corner_points_to_curve(fitted))
    write_log("MSQE is:"+str(msqe_),severity="DEBUG")
    #return feature_vector_from_6points(fitted)+[msqe_]
    
    ps=corner_points_to_curve(fitted)
    ps=[ps[i_]*max(force)/max(n_force) for i_ in range(len(ps))]
    plt.plot(ps,c="red",marker="x")

# END:Taken from other python code:END
#*********************************************************************************
#*********************************************************************************
#*********************************************************************************

steps=50
def get_force_same_resolution(x):
    f=numpy.array(json.loads(x["MEORForce1"]))
    n=len(f)
    v=float(n)/steps
    f1=[0 for i_ in range(steps)]
    for i in range(steps):
        i1=int(math.floor(i*v))
        i2=int(math.floor((i+1)*v))
        f1[i]=numpy.array(f[i1:i2]).sum()/(i2-i1)
    
    ''' Not working now!
    if random.random()<-0.02:
        plt.figure(1)
        plt.plot(f)
        f2=[-100 for i_ in range(n)]
        for i in range(ste):
            f2[i*v:(i+1)*v]=[f1[i] for i_ in range(i*v,(i+1)*v)]
        plt.plot(f)
        plt.plot(f2)
        plt.show()   
    '''
        
    return f1

def avg_force_over_a_stroke(x):
    return numpy.array(x["Force1"]).mean()

df=pandas.read_csv("/home/tamasbence/Msc/weba_temp/weba_step6/features.csv",sep="\t")
#df=df.query("MEName1==43")
#print df.CLDrivePerRecoveryTime.mean()
df["Force1"]=df.apply(lambda x: get_force_same_resolution(x),axis=1)

# Plotting:
plt.figure(1)
color_i=0
marker_i=0
count=0
for name in df["MEName1"].unique():
    crew_points=df.query("MEName1=="+str(name))
    crew_points["AvgForce1"]=crew_points.apply(lambda x: avg_force_over_a_stroke(x),axis=1)
    avg_=crew_points["AvgForce1"].mean()
    std_=crew_points["AvgForce1"].std()
    crew_points=crew_points[crew_points["AvgForce1"] > avg_-0.5*std_]
    count=count+crew_points["Force1"].count()
    
    avgf=[0 for i_ in range(steps)]
    minf=[10000 for i_ in range(steps)]
    for (_,f) in crew_points["Force1"].iteritems():
        avgf=[avgf[i_]+f[i_] for i_ in range(ste)]
        if sum(minf)>sum(f):
            minf=f
        # Other min extracting strategy:
        #minf=[min(minf[i_],f[i_]) for i_ in range(ste)] 
    avgf=[avgf[i_]/crew_points["Force1"].count() for i_ in range(steps)]
    
    #fit_force(numpy.array(avgf))
    plt.plot(avgf,label=str(name),color=colors[color_i])#,marker=markers[marker_i])
    plt.fill_between(range(len(avgf)),avgf,minf,label=str(name),color=colors[color_i],alpha=0.3)#,marker=markers[marker_i])
    
    color_i=color_i+1
    if color_i>=len(colors):
        color_i=0
        marker_i=marker_i+1
        if marker_i>=len(markers):
            marker_i=0
            
    #if color_i>2:
       # break
plt.legend().draggable()
plt.show()
print "Record count="+str(count)
