import pandas
import numpy
import math
import matplotlib.pyplot as plt
import json
import random
import os
import re
from sklearn.decomposition import PCA
import sklearn.preprocessing
import io
import functools
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import silhouette_samples, silhouette_score
from mpl_toolkits.mplot3d import Axes3D

outdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step7/"

pca_comp_count=6

def write_log(text,to_screen_too=False,suffix="\n",severity="INFO"):
    write_log.wr.write("["+severity+"]"+text+suffix)
    if to_screen_too:
        print text
write_log.wr=io.FileIO(outdir+"clustering.log",mode="w")

# Doing reindexing on dataframe
def reindex(x):
    reindex.ind=reindex.ind+1
    return reindex.ind
reindex.ind=-1

# Random cluster label to dataframe
def random_label1(df,n_clusters):
    return int(round(random.random()*n_clusters))

colors=["#f00000","#800000","#00f000","#008000","#0000f0","#000080","#f0f000","#808000","#00f0f0","#008080","#f000f0","#800080","#f0f080","#80f0f0","#f080f0","#000000"]
markers=[".","x","o","s","*","8"]

# Calc penalty points for the clustered data
def penalty_points1(clustered_df):
    false_same=0
    false_different=0
    n=clustered_df["labels"].count()
    write_log("[PENALTY POINTS]Calc false sames...",severity="[DEBUG]")
    for label in clustered_df["labels"].unique():
        if label==-1:
            continue
        #df_label=clustered_df.query("labels=="+str(label))
        df_label=clustered_df[clustered_df["labels"]==label]
        for (_,x1) in df_label["MEName0"].iteritems():
            for (_,x2) in df_label["MEName0"].iteritems():
                if x1!=x2:
                    false_same=false_same+1
    write_log("[PENALTY POINTS]Calc false differents...",severity="[DEBUG]")
    for name in clustered_df["MEName0"].unique():
        if name is None or numpy.isnan(name):
            name=0
        df_name=clustered_df.query("MEName0=="+str(name))
        for (_,x1) in df_name["labels"].iteritems():
            if x1==-1:
                continue
            for (_,x2) in df_name["labels"].iteritems():
                if x1!=x2 and x2!=-1:
                    false_different=false_different+1  
    
    outlier=clustered_df["labels"][clustered_df["labels"] == -1].count()
    points=(1.0*false_same+0.8*false_different+0.5*outlier*outlier)/n/n
    
    write_log("Record count="+str(n),severity="[INFO]")
    write_log("False same="+str(false_same)+"("+str(float(false_same)/n/n)+"%)",severity="[INFO]")            
    write_log("False different="+str(false_different)+"("+str(float(false_different)/n/n)+"%)",severity="[INFO]")
    write_log("Outlier="+str(outlier),severity="[INFO]")
    write_log("Penalty points="+str(points),severity="[INFO]")
    return points

# Calc penalty points for the clustered data
def penalty_points2(clustered_df):
    n=clustered_df.count()[0]
    #treshold=round(n/14 * 0.3)
    penalty_point=0
    for label in clustered_df["labels"].unique():
        df_label=clustered_df[clustered_df["labels"]==label]
        sizes=numpy.array(df_label.groupby(["MEName0"]).size())
        #treshold=round(df_label.count()[0]*0.2)
        #treshold=round(max([max(sizes)*0.65,df_label.count()[0]*0.4]))
        treshold=round(max(sizes)*0.4)
        outlier=0
        clusters=0
        variety_point=0
        for i in range(len(sizes)):
            if sizes[i]<treshold:
                outlier=outlier+sizes[i]
            else:
                clusters=clusters+1 
                variety_point=variety_point+pow(df_label.count()[0]-sizes[i],2)
        if clusters>0:
            variety_point=pow(variety_point/clusters,1.0/2)/df_label.count()[0]
                
        w1=1.0
        w2=1.0
        w3=1.0
        w_label=float(df_label.count()[0])/n
        p=w1*(len(sizes)-clusters)*float(outlier)/df_label.count()[0]+w2*(clusters-1)+w3*variety_point
        penalty_point=penalty_point+w_label*p
        write_log("<penalty points2>Label "+str(label),severity="DEBUG")
        write_log("<penalty points2>"+str(sizes),severity="DEBUG")
        write_log("<penalty points2>n="+str(df_label.count()[0]),severity="DEBUG")
        write_log("<penalty points2>Outlier "+str(outlier),severity="DEBUG")
        write_log("<penalty points2>Clusters "+str(clusters),severity="DEBUG")
        write_log("<penalty points2>Variety point="+str(variety_point),severity="DEBUG")
        write_log("<penalty points2>+points="+str(float(outlier)/df_label.count()[0]+clusters-1),severity="DEBUG")
    write_log("Penalty points="+str(penalty_point),severity="DEBUG")
    return penalty_point

# Dummy points for fast calculation
def penalty_points3(df):
    return 100

# Plotting the results (the clusters)
# Returns the label_to_color mapping
def plot_clusters(df):
    label_to_color={}
    color_i=0
    marker_i=0
    for cluster in df["labels"].unique():
        cluster_points=df.query("labels=="+str(cluster))
        plt.scatter(cluster_points["pca1"],cluster_points["pca2"],label=str(cluster),color=colors[color_i],marker=markers[marker_i])
        label_to_color[cluster]=[colors[color_i],markers[marker_i]]
        color_i=color_i+1
        if color_i>=len(colors):
            color_i=0
            marker_i=marker_i+1
            if marker_i>=len(markers):
                marker_i=0
    plt.title="Clusters"
    return label_to_color
  
# Plotting the results (the different rowers)
def plot_crews(df,label_to_color={},columns_to_plot=["pca1","pca2"]):
    names_to_color={}
    
    names_labels=df.groupby(["MEName0","labels"]).size().order(ascending=False)
    for i in range(names_labels.count()):
        (name,label)=names_labels.index[i]
        if label in label_to_color.keys() and \
            not name in names_to_color.keys():#and not [label_to_color[label] in names_to_color.values()]:
            names_to_color[name]=label_to_color[label]    
    color_i=0
    marker_i=0
    for name in df["MEName0"].unique():
        if name is None or numpy.isnan(name):
            name=0
        crew_points=df.query("MEName0=="+str(name))
        if name in names_to_color.keys():
            [color,marker]=names_to_color[name]
        else:
            [color,marker]=[colors[color_i],markers[marker_i]]
            
        plt.scatter(crew_points[columns_to_plot[0]],crew_points[columns_to_plot[1]],label=str(name),color=color,marker=marker)
        color_i=color_i+1
        if color_i>=len(colors):
            color_i=0
            marker_i=marker_i+1
            if marker_i>=len(markers):
                marker_i=0
    plt.title="Rowers"
    
# Clusters the dataframe based on the given columns (before that: outlier removal, normalization, pca)
# return the penalty points associated with the clustering
def cluster_data(df,columns,plot_result=False):
    if not type(columns) is list:
        columns=columns.tolist()
    write_log("*************************************",severity="[INFO]")
    write_log("Clustering based on columns:"+str(columns),severity="[INFO]")
    # Remove NaN values
    df=df.dropna()
    
    # Outlier removal (based on average forces)
    old_count=df["MEName0"].count()
    write_log("Filtering outliers...",severity="[INFO]")
    crews=[]
    for name in df["MEMeasurementID"].unique(): #TODO: MEName0 ???!!!
        crew_points=df.query("MEMeasurementID=="+str(name)).copy()
        avg_=crew_points["MEAvgForce0"].mean()
        std_=crew_points["MEAvgForce0"].std()
        crew_points=crew_points[crew_points["MEAvgForce0"] > avg_-0.2*std_].copy()
        crews=crews+[crew_points]

    df=pandas.concat(crews)
    df["Index"]=df.apply(lambda x: reindex(x),axis=1)
    df=df.set_index("Index")
    reindex.ind=-1
    
    new_count=df["MEName0"].count()
    write_log("Record count="+str(new_count)+"(original:+"+str(old_count)+",outliers:"+str(old_count-new_count)+").",severity="[INFO]")
    write_log("Different names: "+str(len(df["MEName0"].unique())))
    
    # Remove unused columns
    df_pca=df.copy()
    for col in df.columns:
        if not col in columns:
            df_pca=df_pca.drop(col,axis=1)
    
     # Normalize data (for PCA)
    write_log("Normalizing data...",severity="[INFO]")
    df_arr=sklearn.preprocessing.scale(df_pca)
    '''
    df_norm=pandas.DataFrame(df_arr,columns=df_pca.columns)
    df_norm["CLFullArc0"]=df_norm["CLFullArc0"]*10
    df_norm["CLForce0StabilityAutocorrelation1"]=df_norm["CLForce0StabilityAutocorrelation1"]*2
    '''
    
    # Doing PCA
    write_log("Applying PCA...",severity="[INFO]")
    real_pca_comp_count=min(len(df_pca.columns),pca_comp_count)
    pca = PCA(n_components=real_pca_comp_count)
    pca.fit(df_arr)
    
    # Print out results of PCA
    if real_pca_comp_count>1:
        df_pca_comps=pandas.DataFrame([df_pca.columns]+[pca.components_[i_].tolist() for i_ in range(real_pca_comp_count)]).transpose()
        df_pca_comps=df_pca_comps
        df_pca_comps_columns={0:"name"}
        df_pca_comps_columns.update({int(i_):("pca"+str(i_)) for i_ in range(1,real_pca_comp_count+1)})
        df_pca_comps=df_pca_comps.rename(columns=df_pca_comps_columns)
        write_log("PCA components (>0.1):"+str(df_pca_comps.query("pca1 > 0.1 or pca1 < -0.1")),severity="[INFO]")
        write_log("PCA explained variance ratio:"+str(pca.explained_variance_ratio_),severity="[INFO]")
        write_log("PCA explained variance:"+str(pca.explained_variance_),severity="[INFO]")
    
    # Do the clustering
    pca_columns=["pca"+str(i_) for i_ in range(1,real_pca_comp_count+1)]
    df_pca=pandas.DataFrame(pca.transform(df_arr),columns=pca_columns)
    df_pca["Index"]=df_pca.apply(lambda x: reindex(x),axis=1)
    df_pca.set_index("Index")
    reindex.ind=-1
    
    # Remove outliers with DBSCAN
    write_log("Running DBSCAN for outlier removal...",severity="[INFO]")
    old_count=df_pca.count()[0]
    outlier_removal=DBSCAN(eps=1.0)
    df_pca["outlier_labels"]=outlier_removal.fit_predict(df_pca[pca_columns])
    df_pca=df_pca.query("outlier_labels!=-1")
    new_count=df_pca.count()[0]
    write_log("Record count="+str(new_count)+"(original:+"+str(old_count)+",outliers:"+str(old_count-new_count)+").",severity="[INFO]")
    
    # Clustering
    clustering=DBSCAN(eps=0.8)
    clustering=KMeans(n_clusters=14)
    #clustering=SpectralClustering(n_clusters=14)
    #clustering=AgglomerativeClustering(n_clusters=14,linkage='ward')
    write_log("Clustering...",severity="[INFO]")
    best=[1000,None]
    for i in range(5):
        df_pca["labels"]=clustering.fit_predict(df_pca[pca_columns])
        #df_pca["labels"]=df_pca.apply(lambda x: random_label1(x,n_clusters=14),axis=1)
        df_pca["MEName0"]=df["MEName0"]  
        df_pca["MESide"]=df["MESide"]
    
        # Calc penalty points
        points=penalty_points2(df_pca)
        if points<best[0]:
            best=[points,df_pca.copy()]
            
    #print penalty_points1(best[1])

    points=best[0]
    df_pca=best[1]
    if plot_result:        
        ax=plt.figure(1)
        plt.subplot(211)
        label_to_color=plot_clusters(df_pca)
        plt.legend().draggable()
        plt.subplot(212)
        plot_crews(df_pca,label_to_color,["pca1","pca2"])
        plt.xlabel("pca1")
        plt.ylabel("pca2")
        plt.legend().draggable()
        plt.show()
        plt.figure(1)
        plt.suptitle("Crews - different pca components")
        ax=plt.subplot(321)
        ax.set_xlabel("pca1")
        ax.set_ylabel("pca2")
        plot_crews(df_pca,label_to_color,["pca1","pca2"])
        ax=plt.subplot(322)
        ax.set_xlabel("pca3")
        ax.set_ylabel("pca4")
        plot_crews(df_pca,label_to_color,["pca3","pca4"])
        ax=plt.subplot(323)
        ax.set_xlabel("pca5")
        ax.set_ylabel("pca6")
        plot_crews(df_pca,label_to_color,["pca5","pca6"])
        ax=plt.subplot(324)
        ax.set_xlabel("pca1")
        ax.set_ylabel("pca3")
        plot_crews(df_pca,label_to_color,["pca1","pca3"])
        ax=plt.subplot(325)
        ax.set_xlabel("pca1")
        ax.set_ylabel("pca4")
        plot_crews(df_pca,label_to_color,["pca1","pca4"])
        ax=plt.subplot(326)
        ax.set_xlabel("pca2")
        ax.set_ylabel("pca3")
        plot_crews(df_pca,label_to_color,["pca2","pca3"])
        plt.show()
    
    return points

def load_data(frac=None,side=None):
    df=pandas.read_csv("/home/tamasbence/Msc/weba_temp/weba_step6/features_only_one_side.csv",sep="\t")
    df=df.dropna()
    if not side is None:
        df=df[df["MESide"]==side]
    
    columns=["CLFullArc0","CLEffectiveArc0","CLAverageForce12Diff","CLDrivePerRecoveryTime"]
    columns=columns+["CLForce0W1","CLForce0W2","CLForce0W3","CLForce0W4","CLForce0W5","CLForce0Slope2","CLForce0Slope4"]
    #columns=columns+["CLForce0StabilityAutocorrelation1","CLForce0StabilityMSQE1","CLForce0StabilityAutocorrelation2","CLForce0StabilityMSQE2"]
    for col in columns:
        if re.search("^CL",col)!=None:
            avg_=df[col].mean()
            std_=df[col].std()
            mult=3.0
            df=df[df[col]>avg_-mult*std_][df[col]<avg_+mult*std_]
    
    if not frac is None:
        df=df.sample(frac=frac)
    df["index"]=df.apply(lambda x: reindex(x),axis=1)
    df=df.set_index("index")
    reindex.ind=-1
    write_log("Record count="+str(df["MEName0"].count()),severity="[INFO]")
    return df

def search_good_features(df,columns_to_try):
    to_beat=[100,[]]
    exiting=False
    while not exiting:
        bs=[100,None]
        for col in df.columns:
            if not col in to_beat[1] and col in columns_to_try and re.search("^CL",col)!=None:
                reindex.ind=-1
                write_log("Trying column "+col+"...",to_screen_too=True,severity="[INFO1]")
                temp_points=cluster_data(df,to_beat[1]+[col],plot_result=False)
                if temp_points < bs[0]:
                    write_log("This is better, adding "+col+" to columns.",severity="[INFO]")
                    bs=[temp_points,col]
                else:
                    write_log( "This is not better.",severity="[INFO]")
        if to_beat[0] > bs[0]+0.0005:
            write_log("The best columns in this round:"+bs[1]+".",severity="[INFO1]",to_screen_too=True)
            to_beat=[bs[0],to_beat[1]+[bs[1]]]
        else:
            exiting=True
            write_log("The penalty point gain treshold is not met. Exiting search.",severity="[INFO1]",to_screen_too=True)
        
    write_log("Best result:"+str(to_beat[0]),severity="[INFO1]",to_screen_too=True)
    write_log("     with columns:"+str(to_beat[1]),severity="[INFO1]",to_screen_too=True)
    
def search_good_features_loop(df,columns_to_try,loop=3):
    for i in range(loop):
        search_good_features(df,columns_to_try)

# Best columns:
#side1:columns=['CLFullArc0', 'CLForce0Part3', 'CLFullLength_0', 'CLForce0Part2', 'CLForce0Diff8', 'CLEffectiveArc0']
#side2(only fewer columns tried):columns=['CLEffectiveArc0', 'CLFullLength_0', 'CLForce0Diff9']
#side2(all columns tried):columns=['CLFullArc0', 'CLAverageHandleSpeed', 'CLDegForce0DownFrom40', 'CLForce0StabilityAutocorrelation1', 'CLFullLength_0', 'CLEffectiveLength0']
#side both(only fewer columns tried):columns=['CLFullArc0', 'CLFullLength_0', 'CLForce0Diff9', 'CLForce0Part3', 'CLEffectiveArc0', 'CLForce0Diff8']
#side both(all columns tried):columns=['CLFullArc0', 'CLAverageHandleSpeed', 'CLReleaseArc0W2', 'CLFullLength_0', 'CLEffectiveLength0', 'CLDegForce0DownFrom40', 'CLForce0StabilityAutocorrelation1', 'CLAvgPerMaxForce0']
#side both(all columns tried,except release arc):
columns=['CLFullArc0', 'CLAverageHandleSpeed', 'CLFullLength_0', 'CLEffectiveLength0', 'CLDegForce0DownFrom40', 'CLForce0StabilityAutocorrelation1', 'CLAvgPerMaxForce0']

# Run one clustering
#for i in range(4):
print cluster_data(load_data(side=None,frac=0.95),columns,plot_result=True)

write_log.wr.close()

'''
# Trying out features - brute force strategy
write_log.wr=io.FileIO(outdir+"clustering_side2_1.log",mode="w")

columns_to_try=["CLFullArc0","CLAvgArc12Diff","CLFullLength_0","CLEffectiveArc0"]
columns_to_try=columns_to_try+["CLAverageForce12Diff"]
columns_to_try=columns_to_try+["CLDrivePerRecoveryTime"]
columns_to_try=columns_to_try+["CLForce0W2","CLForce0W3","CLForce0W4","CLForce1Slope2","CLForce1Slope4"]
columns_to_try=columns_to_try+["CLDegForce0UpTo70","CLDegForce0UpTo70"]
columns_to_try=columns_to_try+["CLDegForce0UpTo40","CLDegForce0UpTo40","CLPeakForce0AtLengthPercent"]
columns_to_try=columns_to_try+["CLForce0Part1","CLForce0Part2","CLForce0Part3","CLForce0Part4","CLForce0Part5"]
columns_to_try=columns_to_try+["CLForce0Diff1","CLForce0Diff2","CLForce0Diff3","CLForce0Diff4","CLForce0Diff5","CLForce0Diff6","CLForce0Diff7","CLForce0Diff8","CLForce0Diff9"]

search_good_features_loop(load_data(side=2),columns_to_try)

write_log.wr.close()
write_log.wr=io.FileIO(outdir+"clustering_side2_2.log",mode="w")

columns_to_try=[]
for col in df.columns:
    if re.search("^CL",col):
        columns_to_try=columns_to_try+[col]
        
search_good_features_loop(load_data(side=2),columns_to_try)

write_log.wr.close()
write_log.wr=io.FileIO(outdir+"clustering_sideboth_1.log",mode="w")

columns_to_try=["CLFullArc0","CLAvgArc12Diff","CLFullLength_0","CLEffectiveArc0"]
columns_to_try=columns_to_try+["CLAverageForce12Diff"]
columns_to_try=columns_to_try+["CLDrivePerRecoveryTime"]
columns_to_try=columns_to_try+["CLForce0W2","CLForce0W3","CLForce0W4","CLForce1Slope2","CLForce1Slope4"]
columns_to_try=columns_to_try+["CLDegForce0UpTo70","CLDegForce0UpTo70"]
columns_to_try=columns_to_try+["CLDegForce0UpTo40","CLDegForce0UpTo40","CLPeakForce0AtLengthPercent"]
columns_to_try=columns_to_try+["CLForce0Part1","CLForce0Part2","CLForce0Part3","CLForce0Part4","CLForce0Part5"]
columns_to_try=columns_to_try+["CLForce0Diff1","CLForce0Diff2","CLForce0Diff3","CLForce0Diff4","CLForce0Diff5","CLForce0Diff6","CLForce0Diff7","CLForce0Diff8","CLForce0Diff9"]

search_good_features_loop(load_data(),columns_to_try)

write_log.wr.close()
write_log.wr=io.FileIO(outdir+"clustering_sideboth_2.log",mode="w")

columns_to_try=[]
for col in df.columns:
    if re.search("^CL",col):
        columns_to_try=columns_to_try+[col]
        
search_good_features_loop(load_data(),columns_to_try)       
'''

write_log.wr.close()
