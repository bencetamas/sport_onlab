    def binarize_name(x,name):
        if x["MEName0"]==name:
            return 1
        else:
            return 0
            
    cols=['CLFullArc0', 'CLAverageHandleSpeed', 'CLFullLength_0', 'CLEffectiveLength0', 'CLDegForce0DownFrom40', 'CLForce0StabilityAutocorrelation1', 'CLAvgPerMaxForce0']
    for col in ['CLFullArc0', 'CLAverageHandleSpeed', 'CLFullLength_0', 'CLEffectiveLength0', 'CLDegForce0DownFrom40', 'CLForce0StabilityAutocorrelation1', 'CLAvgPerMaxForce0']:
        df_pca[col]=df[col]
    name_cols=[]
    for name in df_pca["MEName0"].unique():
        name_cols=name_cols+["MEName0is"+str(name)]
        df_pca["MEName0is"+str(name)]=df_pca.apply(lambda x: binarize_name(x,name),axis=1)
    df_pca[name_cols].to_csv(outdir+"checking.csv",header=True,sep="\t")
    print cols
    df_pca.corr()[cols].loc[cols].to_csv(outdir+"correlation1.csv",header=True,sep="\t")
    print name_cols
    print cols
    print df_pca.corr()[name_cols].loc[cols].to_csv(outdir+"correlation2.csv",header=True,sep="\t")
    print df_pca.corr().to_csv(outdir+"correlation3_raw.csv",header=True,sep="\t")
