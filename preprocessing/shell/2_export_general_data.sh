#!/bin/bash

#cd "/media/tamasbence/D4CA98B3CA9892FA1/Documents and Settings/tamasbence/Documents/egyetem/onlab_rowing/dm_rowing/webapersonal files/"
while IFS='' read -r line || [[ -n "$line" ]]; do
	j="${line}.pdb";
	echo "Processing $j";
	# Get variables from filename
	lastname=$(echo $j | sed 's/\([A-Z]*[0-9]*\)_.*/\1/');
	if [ $lastname == "NOVAK" ]; then
		lastname="NOVÁK";
	else if [ $lastname == "JUHASZ" ]; then
		lastname="JUHÁSZ";
	else if [ $lastname == "SZABO" ]; then
		lastname="SZABÓ";
	else if [ $lastname == "RACZ" ]; then
		lastname="RACZ";
	else if [ $lastname == "VINKO" ]; then
		lastname="VINKÓ";
	else if [ $lastname == "ISMERETLEN1" ]; then
		lastname="SMITH";
	else if [ $lastname == "SZELL" ]; then
		lastname="SZÉLL";
	else if [ $lastname == "SZEKER" ]; then
		lastname="SZEKÉR";
	fi fi fi fi fi fi fi fi
	firstname=$(echo $j | sed 's/[A-Z]*_\([^_]*\)_.*/\1/');
	datetime=$(echo $j | sed 's/.*_\([0-9][0-9][0-9][0-9]\)\([0-9][0-9]\)\([0-9][0-9]\)_\([0-9]*\).*/\3\2\1_\4/');
	echo "${lastname} ${firstname} at ${datetime}";

	# Acquire apropriate filename
	file=$(ls | grep $lastname | grep $datetime | grep ".pdb");
	if [ -z "$file" ]; then
		#echo "not found ${file}";
		datetime=$(echo $datetime | sed 's/\(........_....\)00/\1/');
		file=$(ls | grep $lastname | grep $datetime | grep ".pdb");
	fi
	echo $file;

	# Export things from MS ACCESS database (from GENERAL table)
        # Schema: LastName,FirstName,Date,SubDeviceType,Group,Birthyear,SportSince,Sex,Height,Weight,Comment,SpecialFields
	schema=$(echo "LastName,FirstName,Date,SubDeviceType,Group,Birthyear,SportSince,Sex,Height,Weight,Comment,OarLength,OarInboard" | tr ',' "\t");
	out1=$(mdb-export -H -d '\t' -Q -D "%Y-%m-%d %H:%M:%S" "$file" GENERAL | sed 's/ROBERT.*BENCE/ROBERT/' | sed 's/\([1-9]\)\.\([0-9]\)[0-9]*e+[0-9]*../\1\2/');
	out2=$(mdb-export -H -b octal "$file" GENERAL | sed 's/.*\"\\000\\000\\000.\([0-9]*\).\([0-9]*\)\"/\1\t\2/');

	# Write output
	out_file="../weba_step6/${line}";
	echo $schema >> $out_file;
	echo -e "${out1}"'\t'"${out2}" >> $out_file;

	# Some output
	echo ""
	echo ""
done < "/media/tamasbence/D4CA98B3CA9892FA1/Documents and Settings/tamasbence/Documents/egyetem/onlab_rowing/dm_rowing/MEASUREMENT_LIST.TXT"
