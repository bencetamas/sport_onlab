workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step1/"
outdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step2/"

# Singles
class ImportType:
    Pairs, Singles = range(2)      
importType=ImportType.Singles
session_file_sub_dir="single_sessions/"
auxiliary_file_sub_dir="single_auxiliary/"
analysis_file_sub_dir="single_analysis/"
'''
# Pairs
class ImportType:
    Pairs, Singles = range(2)      
importType=ImportType.Pairs
session_file_sub_dir="pair_sessions/"
auxiliary_file_sub_dir="pair_auxiliary/"
analysis_file_sub_dir="pair_analysis/"
'''

import sys
import datetime
# import hashlib
import math
import pandas as pd
import numpy
from os import walk
import re

# Decodes the filename into a name, a datetime, and a session ID part
def decode_filename(filename):
    name=re.search("([A-Z]*_[A-Z]*)|([A-Z0-9]*)",filename).group(0)
    date=re.search("[0-9][[0-9][0-9]*_[0-9]*",filename).group(0)
    session=re.search("(?<=_S)[0-9][0-9]?",filename).group(0)
    return (name,date,session)
    
# Finds the another file for this measurement (in case of a single, it is the same)
def find_pair_file(f1):
    (name1,date,session)=decode_filename(f1)
    if importType==ImportType.Pairs:
        found_pair=False
        f2=""
        for f2 in filenames:
            if (re.search("(?<!"+name1+"_"+")"+date+"_S"+session+"\.dat",f2) != None):
                (name2,_,_)=decode_filename(f2)
                if name1<name2:
                    found_pair=True
                    break
        return (found_pair,f2)
    else:
        return (True,f1)
    

# Mapping names to IDs
def name_to_id(name):
    # This is to long to use in every row
    #return hashlib.sha1(name.encode()).hexdigest()[0:10]+str(datetime.datetime.now().time())[9:12]
    if not hasattr(name_to_id,"counter"):
        name_to_id.counter=0
    else:
        name_to_id.counter+=1
    return str(name_to_id.counter).zfill(2)

# Swap the crew related fields contents in the dataframe
def swap_crew(df):
    for col in ["Name","Weight","Height","Force","Arc"]:
        df.loc[df.index,[col+"1",col+"2"]] = df.loc[df.index,[col+"2",col+"1"]].values
    return df
       
# Extracting timedelta (integer in milliseconds)
def extract_time(x,column,time_format="%M:%S:%f"):
    if x[column]=="--":
        return 0
    #return datetime.datetime.strptime(x["Time total (1) [h:mm:ss:00]"],"%H:%M:%S:%f")-base_time
    if type(x[column])!=str:
        print str(x[column]) +","+column
    td=datetime.datetime.strptime(x[column],time_format)-extract_time.base_time
    return td.seconds*1E3+td.microseconds/1E3
extract_time.base_time=datetime.datetime.strptime("00","%S")

additional_analysis_columns_pair=["Drive time[mm:ss:00]","Recovery time[mm:ss:00]"]
additional_analysis_columns_single=["Drive time (1) [mm:ss:00]","Recovery time (1) [mm:ss:00]"]
if importType==ImportType.Pairs:
    additional_analysis_columns=additional_analysis_columns_pair
else:
    additional_analysis_columns=additional_analysis_columns_single

# Extends the analysis dataframe by duplicating rows to fit the basic dataframe's row count
def extend_analysis_df(analysis,start_time,time_delta,time_delta_float):
    stroke_times=analysis.apply(lambda x: extract_time(x,"Time per stroke[mm:ss:00]") ,axis=1)
    if stroke_times[stroke_times == 0].count>0:
        print "WARNING: there are empty rows in analysis file!"
    # Additional columns:
    drive_times=analysis.apply(lambda x: extract_time(x,additional_analysis_columns[0]) ,axis=1)
    recovery_times=analysis.apply(lambda x: extract_time(x,additional_analysis_columns[1]) ,axis=1)

    #print str(start_time)+"+nx"+str(time_delta_float)
    print "Extending analysis"

    rest=0
    index=0
    ext_list=[]
    time=start_time
    if stroke_times.empty:
        ext_list=[[0,0,0,0,0]]
        print "WARNING:analysis file is empty"
    else:
        for i in range(0,stroke_times.count()):
            iterations=(stroke_times[i]+rest)/time_delta_float
            fiterations=math.floor(iterations)
            rest=iterations-fiterations
            for _ in range(0,int(fiterations)):
                ext_list+=[[str(time)[11:-3],i,drive_times[i],recovery_times[i],stroke_times[i]]]
                time+=time_delta
            index+=int(fiterations)
            if i%100==0:
                sys.stdout.write(".")
    print ""
    return pd.DataFrame(ext_list,columns=["Time","StrokeNumber","DriveTime","RecoveryTime","StrokeTime"])

def prefix_columns(df,prefix,columns_to_exclude=[],suffix=None):
    old_to_new={}
    for col in df.columns:
        if not col in columns_to_exclude:
            old_to_new[col]=prefix+col+suffix
    return df.rename(columns=old_to_new)


# All the columns, which are in the analysis table
analysis_columns_pair=["Strokes number[Nr]","Drive time[mm:ss:00]","Recovery time[mm:ss:00]","Rhythm[%]","Strokes per minute[1/min]",
                  "Time per stroke[mm:ss:00]","Time total[h:mm:ss:00]","Force per stroke[N]","Force average[N]",
                  "Force per stroke Xaxis[N]","Force average Xaxis[N]","Force disipation (Yaxis)[%]","Force peak[N]",
                  "Fstr/Fpeak[%]","Fstr/Body weight[N/Kg]","Time to force peak[mm:ss:00]","Angle to force peak[deg]",
                  "Force in phase1[N]","Force in phase2[N]","Force in phase3[N]","Persent in phase1[%]",
                  "Persent in phase2[%]","Persent in phase3[%]","Power per stroke (drive time)[W]",
                  "Power average (drive time)[W]","Power per stroke[W]","Power per stroke in X dir.[W]",
                  "Power average in X dir.[W]","Power average[W]","Work per stroke[J]","Work average[KJ]",
                  "Arc[deg]","Catch arc[deg]","Release arc[deg]","Arc effective[deg]","Catch slip drive[deg]",
                  "Release slip drive[deg]","Boat velocity average[m/s]","Boat velocity peak[m/s]",
                  "Boat velocity min[m/s]","BVpeak/BVmin[%]","BVavr/BVpeak[%]","AccelX average[m/s^2]",
                  "AccelX +peak[m/s^2]","AccelX -peak[m/s^2]","AccelX -time[mm:ss:00]","AccelZ average[m/s^2]",
                  "AccelZ +peak[m/s^2]","AccelZ -peak[m/s^2]","AccelZ -time[mm:ss:00]","Distance total[m]",
                  "Distance per stroke[m]","Handle distance[m]","Handle velocity[m/s]","Heart rate"]
analysis_columns_single=["Strokes number[Nr]","Strokes per minute[1/min]","Time per stroke[mm:ss:00]",
                         "Time total [h:mm:ss:00]","Distance per stroke [m]","Distance total[m]",
                         "Force per stroke[N]","Force average[N]","Force per stroke Xaxis[N]",
                         "Force average Xaxis[N]","Power per stroke[W]","Power average[W]","Power per stroke Xaxis[W]",
                         "Power average Xaxis[W]","Work per stroke[J]","Work average[KJ]","Heart rate[1/min]",
                         "Drive time (1) [mm:ss:00]","Recovery time (1) [mm:ss:00]","Rhythm (1) [%]",
                         "Force per stroke (1) [N]","Force average (1) [N]","Force per stroke Xaxis (1) [N]",
                         "Force average Xaxis (1) [N]","Force disipation (Yaxis) (1) [%]","Force peak (1) [N]",
                         "Fstr/Fpeak (1) [%]","Fstr/Body weight (1) [N/Kg]","Time to force peak (1) [mm:ss:00]",
                         "Angle to force peak (1) [deg]","Force in phase1 (1) [N]","Force in phase2 (1) [N]",
                         "Force in phase3 (1) [N]","Persent in phase1 (1) [%]","Persent in phase2 (1) [%]",
                         "Persent in phase3 (1) [%]","Power per stroke (drive time) (1) [W]","Power average (drive time) (1) [W]",
                         "Power per stroke (1) [W]","Power average (1) [W]","Power per stroke in X dir. (1) [W]",
                         "Power average in X dir. (1) [W]","Work per stroke (1) [J]","Work average (1) [KJ]",
                         "Arc (1) [deg]","Catch arc (1) [deg]","Release arc (1) [deg]","Arc effective (1) [deg]","Catch slip drive (1) [deg]",
                         "Release slip drive (1) [deg]","Boat velocity average (1) [m/s]","Boat velocity peak (1) [m/s]",
                         "Boat velocity min (1) [m/s]","BVpeak/BVmin (1) [%]","BVavr/BVpeak (1) [%]","AccelX average (1) [m/s^2]",
                         "AccelX +peak (1) [m/s^2]","AccelX -peak (1) [m/s^2]","AccelX -time (1) [mm:ss:00]",
                         "AccelZ average (1) [m/s^2]","AccelZ +peak (1) [m/s^2]","AccelZ -peak (1) [m/s^2]",
                         "AccelZ -time (1) [mm:ss:00]","Handle distance (1) [m]","Handle velocity (1) [m/s]",
                         "Drive time (2) [mm:ss:00]","Recovery time (2) [mm:ss:00]","Rhythm (2) [%]",
                         "Force per stroke (2) [N]","Force average (2) [N]","Force per stroke Xaxis (2) [N]",
                         "Force average Xaxis (2) [N]","Force disipation (Yaxis) (2) [%]","Force peak (2) [N]",
                         "Fstr/Fpeak (2) [%]","Fstr/Body weight (2) [N/Kg]","Time to force peak (2) [mm:ss:00]",
                         "Angle to force peak (2) [deg]","Force in phase1 (2) [N]","Force in phase2 (2) [N]",
                         "Force in phase3 (2) [N]","Persent in phase1 (2) [%]","Persent in phase2 (2) [%]",
                         "Persent in phase3 (2) [%]","Power per stroke (drive time) (2) [W]","Power average (drive time) (2) [W]",
                         "Power per stroke (2) [W]","Power average (2) [W]","Power per stroke in X dir. (2) [W]",
                         "Power average in X dir. (2) [W]","Work per stroke (2) [J]","Work average (2) [KJ]","Arc (2) [deg]",
                         "Catch arc (2) [deg]","Release arc (2) [deg]","Arc effective (2) [deg]","Catch slip drive (2) [deg]",
                         "Release slip drive (2) [deg]","Boat velocity average (2) [m/s]","Boat velocity peak (2) [m/s]",
                         "Boat velocity min (2) [m/s]","BVpeak/BVmin (2) [%]","BVavr/BVpeak (2) [%]","AccelX average (2) [m/s^2]",
                         "AccelX +peak (2) [m/s^2]","AccelX -peak (2) [m/s^2]","AccelX -time (2) [mm:ss:00]",
                         "AccelZ average (2) [m/s^2]","AccelZ +peak (2) [m/s^2]","AccelZ -peak (2) [m/s^2]",
                         "AccelZ -time (2) [mm:ss:00]","Handle distance (2) [m]","Handle velocity (2) [m/s]","Heart rate"]
if importType==ImportType.Pairs:
    analysis_columns=analysis_columns_pair
else:
    analysis_columns=analysis_columns_single
    
# Rename columns
def column_rename(df,new_names):
    column_rename_dict={}                
    for i in range(len(df.columns)):
        column_rename_dict[df.columns[i]] = new_names[i]
    return df.rename(columns=column_rename_dict)

# Merges the content of the df dataframe with the derived attributes, calculated in the files(array) analysis files
# the new attributes have a prefix and a suffix(=crew number)
def merge_with_analysis(df,files):
    start_time=datetime.datetime.strptime(df.loc[0].Time,"%H:%M:%S.%f")
    time_delta=datetime.datetime.strptime(df.loc[1].Time,"%H:%M:%S.%f")-start_time
    time_delta_float=time_delta.seconds*1E3+time_delta.microseconds/1E3
    res=df
    
    i=1
    for afile in files:
        analysis=pd.read_csv(afile,
                        sep="\t",
                        header=0)#,
                        #index_col=None,
                        #names=analysis_columns)
        analysis=column_rename(analysis, analysis_columns)
        
        extended_analysis=extend_analysis_df(analysis,start_time,time_delta,time_delta_float)
        extended_analysis=prefix_columns(extended_analysis,"WebaD",["Time"],"("+str(i)+")")
        res=pd.merge(res,extended_analysis,on=["Time"],how='left')
        
        i+=1
        
    return res

# Returns the exact downsample rate (based on guess) or returns None, if ambigous
def round_sample_rate(rate,force_return=False):
    if rate > 1 and rate < 3:
        return 2
    elif rate > 3 and rate < 5.2:
        return 4
    elif rate > 6.2 and rate < 12:
        return 8
    elif rate > 15 and rate < 26:
        return 20
    elif force_return:
        if rate < 3:
            return 2
        elif rate < 6:
            return 4
        elif rate < 14:
            return 8
        else:
            return 20
    else:
        return None
    

# Returns the number of similar consecutive rows (there are duplicate rows, only time differs)
def calc_downsampling_rate(df):
    sample_decision_treshold=0.3
    sampling_attrs=["Force1","Force2","Arc1","Arc2","Velocity"]
    
    sample_rate=1
    for attr in sampling_attrs:
        if not attr in df.columns:
            continue
        a=numpy.array(df[attr])
        i=0
        current_sample=a[i]
        current_sample_valid=False
        current_sample_rate=0
        sample_rate=0
        rate_n=0
        while i<a.size and rate_n<150:
            current_sample_rate=current_sample_rate+1
            if a[i]!=current_sample and current_sample_valid:
                sample_rate=sample_rate+current_sample_rate
                rate_n=rate_n+1
                current_sample=a[i]
                current_sample_rate=0
                current_sample_valid=(a[i]!=0)
            if not current_sample_valid and a[i]!=0:
                current_sample=a[i]
                current_sample_rate=0
                current_sample_valid=True 
            i=i+1
        if rate_n>0:
            sample_rate=float(sample_rate)/rate_n
        else:
            sample_rate=0
        if (round_sample_rate(sample_rate) != None):
            print "Downsampling rate is: "+str(sample_rate)
            return round_sample_rate(sample_rate)
        print "Downsampling rate based on "+attr+" is ambigous ("+str(sample_rate)+"). Trying another attribute."
    return round_sample_rate(sample_rate,force_return=True)

# Automatically detects duplicating and downsamples data to remove them
def downsample(df):
    sample_rate=calc_downsampling_rate(df)
    res=df[::sample_rate]
    res.index =range(0,res.count()[0])
    return res

# Loads the basic attributes from files and merges them to one dataframe
def merge_basic_data(f1,f2):
    if importType==ImportType.Pairs:
        schema1=["Time","Force1","Arc1","AccX","AccZ","Velocity","Heart1"]
        schema2=["Time","Force2","Arc2","AccX2","AccZ2","Velocity2","Heart2"]
    else:
        schema1=["Time","Force1","Arc1","Force2","Arc2","AccX","AccZ","Velocity","Heart1"]
        schema2=["Time","Force1_2","Arc1_2","Force2_2","Arc2_2","AccX2","AccZ2","Velocity2","Heart2"]
    df1=downsample(pd.read_csv(workdir+session_file_sub_dir+f1,
                        sep="\t",
                        header=1,
                        names=schema1))
    df2=downsample(pd.read_csv(workdir+session_file_sub_dir+f2,
                        sep="\t",
                        header=1,
                        names=schema2))
    
    # Handle special case
    # VERMES KREPESICS 20100408 181308 file is works only with this (time is not matching)
    if (re.search("VERMES",f1+f2) != None and 
        re.search("KRPESICS",f1+f2) != None and 
        re.search("20100408_181308",f1+f2) != None):
        print "Handling special case, renaming Time column.(VERMES-KRPESICS"
        df2=df2.rename(columns={"Time":"Time2"})
        x= df1.join(df2)
        return x
    
    return pd.merge(df1,df2,on=["Time"])

# Checks for anomalies (eg. not the same values in the two file)
def noAnomaliesAfterMerge(df,f1,f2):
    df["DeltaAccX"]=df["AccX"]-df["AccX2"]
    df["DeltaAccZ"]=df["AccZ"]-df["AccZ2"]
    df["DeltaVelocity"]=df["Velocity"]-df["Velocity2"]
    filterExpr = "(DeltaAccX > 0.0001 or DeltaAccX < -0.001) and"
    filterExpr+= "(DeltaAccZ > 0.0001 or DeltaAccZ < -0.001) and"
    filterExpr+= "(DeltaVelocity > 0.0001 or DeltaVelocity < -0.001)"
    if df.query(filterExpr)["Time"].count() > 0:
        print "ERROR:There are differences beetwen the two measured velocity/acceleration. Check the files manually!"
        print "("+f1+","+f2+"):"
        print df.query(filterExpr).head(20)
        return False
    else:
        return True
    
# Loads auxiliary info from the given files, and merge it with current dataframe
def load_and_merge_auxiliary(df,basic_files,aux_files,names):
    [f1,f2]=basic_files
    [aux_file1,aux_file2]=aux_files
    [name1,name2]=names
    aux1=pd.read_csv(aux_file1,
                      sep="\t",
                      index_col=False,
                      header=0)
    aux2=pd.read_csv(aux_file2,
                      sep="\t",
                      index_col=False,
                      header=0)
    if aux1.loc[0].OarInboard != aux2.loc[0].OarInboard or \
       aux1.loc[0].OarLength != aux2.loc[0].OarLength or \
           aux1.loc[0].Sex != aux2.loc[0].Sex:
        print "ERROR:Sex, oar inboard or oar length does not match on the two sides!"
        print "("+f1+","+f2+")"
        print str(aux1.loc[0].OarInboard)+"/"+str(aux2.loc[0].OarInboard)+" and "+ \
                str(aux1.loc[0].OarLength)+"/"+str(aux2.loc[0].OarLength)+" and "+ \
                str(aux1.loc[0].Sex)+"/"+str(aux2.loc[0].Sex)
    else:
        df["Name1"]=names_ids[name1]
        df["Name2"]=names_ids[name2]
        df["Date"]=aux1.loc[0].Date[0:10]
        df["OarInboard"]=aux1.loc[0].OarInboard
        df["OarLength"]=aux1.loc[0].OarLength
        df["Sex"]=aux1.loc[0].Sex
        df["Weight1"]=aux1.loc[0].Weight
        df["Height1"]=aux1.loc[0].Height
        df["Weight2"]=aux2.loc[0].Weight
        df["Height2"]=aux2.loc[0].Height
                 
        # Check If something wrong with the crew order in aux files
        if (aux1.loc[0].Group == 2 and aux2.loc[0].Group ==1 and importType==ImportType.Pairs):
            df=swap_crew(df)
            tmp=name1
            name1=name2
            name2=tmp
        elif (aux1.loc[0].Group == 1 and aux2.loc[0].Group ==2 and importType==ImportType.Pairs):
            pass
        elif (aux1.loc[0].Group == 1 and aux2.loc[0].Group ==1 and importType==ImportType.Singles):
            pass
        else:
            print "WARNING: something wrong in the crew order, please check it!"
            print "("+f1+","+f2+")"
                    
        return [df,name1,name2]
    
# Writes the id of the measurement and the session to dataframe
def write_measurement_id(df,measurement_id,session_id):
    df["MeasurementID"] = measurement_id
    df["SessionID"] = session_id
    return df

# Columns after merging the basic attributes
merged_columns = ["Time","Force1","Force2","Arc1","Arc2","AccX","AccZ","Velocity"]
# Additional columns(the same value in all row): the names and data from auxiliary files
additional_columns = ["MeasurementID","SessionID","Name1","Name2","Date","Sex","Weight1","Weight2","Height1","Height2","OarInboard","OarLength"]
# Names to IDs mapping
names_ids={}
#names_ids={"VERMES_PETER":16,"KRPESICS_PETER":15}

# Getting the input filenames
filenames = []
for (dirpath, dirname, f) in walk(workdir+session_file_sub_dir):
    filenames.extend(f)
    break

# The filename: an integer number
fout=0

for f1 in filenames:
    (name1,date,session)=decode_filename(f1)
    #if re.search("KRPESICS.*20100408_181308.*", f1) == None:
    #    continue
    (found_pair, f2)=find_pair_file(f1)
    if (not found_pair):
        print "Not found a pair! ("+f1+")"
    else:
        print "Merging "+f1+" and "+f2+"."
        df=merge_basic_data(f1,f2)
        if noAnomaliesAfterMerge(df,f1,f2):
            # Create output dataframe, from existing data
            out=pd.DataFrame(columns=additional_columns+merged_columns,index=df.index);
            out.loc[df.index,merged_columns]= df.loc[df.index,merged_columns]
            
            # Map names to IDs
            name2=re.search("([A-Z]*_[A-Z]*)|([A-Z0-9]*)",f2).group(0)
            for name in [name1,name2]:
                if not name in names_ids:
                    names_ids[name]=name_to_id(name)
                    
            # Auxiliary and Weba derived data
            print "Merging auxiliary files"
            [out,name1,name2]=load_and_merge_auxiliary(out,[f1,f2],
                                     [workdir+auxiliary_file_sub_dir+name1+"_"+date,
                                      workdir+auxiliary_file_sub_dir+name2+"_"+date],
                                     [name1,name2])
            print "Merging analysis files"
            out=merge_with_analysis(out,
                                        [workdir+analysis_file_sub_dir+name1+"_"+date+"_A"+session+".dat",
                                        workdir+analysis_file_sub_dir+name2+"_"+date+"_A"+session+".dat"])
               
            # Writing out
            fout+=1
            out=write_measurement_id(out,fout,session)
            #print out.head(20)
            out.to_csv(outdir+str(fout).zfill(3)+".csv",header=True,index=False,sep='\t',date_format="%H:%M:%S:%MS")
    #break
ndf=pd.DataFrame(names_ids.items(),columns=["Name","ID"])
#print ndf
ndf.to_csv(outdir+"name_id_mapping.csv",header=True,index=False,sep='\t')
