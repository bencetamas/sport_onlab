workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step5/"
outdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step6/"

attributes=["Arc1","Arc2","Force1","Force2","Velocity","AccX","AccZ"]
prefixes=["","Catch","Release"]

import pandas
import numpy
import matplotlib.pyplot as plt
import json
import random
import os
import re

# List files
filenames=[]
for (dirname,_,files) in os.walk(workdir):
    for f in files:
        if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None:
            filenames=filenames+[dirname+"/"+f]
filenames.sort()

for f in filenames:
    df=pandas.read_csv(f,sep="\t")
    
    for idx in range(df.MeasurementID.count()):
        if random.random() < 0.05:
            plt.figure(1)
            i=1
            for pref in prefixes:
                plt.subplot(len(prefixes)*100+10+i)
                for attr in attributes:
                    arr=numpy.array(json.loads(df[pref+attr][idx]))
                    plt.plot(arr/(arr.max()-arr.min()),label=attr)
                plt.legend()
                plt.title="Stroke:"+pref
                i=i+1
            plt.show()
