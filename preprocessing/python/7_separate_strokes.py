workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step4/"
outdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step5/"

max_stroke_time_ms=3550
min_stroke_time_ms=1300

import pandas
import numpy
import os
import re
import math
import datetime
import matplotlib.pyplot as plt
import json
import random
import io

def write_log(text,to_screen_too=False,suffix="\n"):
    write_log.wr.write(text+suffix)
    if to_screen_too:
        print text
write_log.wr=io.FileIO(outdir+"extract_strokes.log",mode="w")

def sgn_(x,offset=0):
    if offset < 0 and x > -offset*sgn_.offset_calib:
        return 1
    elif offset > 0 and x < offset*sgn_.offset_calib:
        return -1
    elif x > -offset*sgn_.offset_calib:
        return 1
    elif x < offset*sgn_.offset_calib:
        return -1
    else:
        return 0
sgn_.offset_calib=0.0

# Wrapper around plt.plot, dont plot, if defined
def plot(x,y=None,c="r"):
    if plot.enabled:
        if y is None:
            y=x
            x=range(len(y))
        plt.plot(x,y,c)
plot.enabled=False
plot.probability=0.05

# The codes to assign to different detected phases, based on the current phase
def phase1_code(current_phase):
    if current_phase==1:
        return 'd'
    else:
        return 'r'
def phase2_code(current_phase):
    return phase1_code(current_phase*-1)

# The colors to assign to different detected phases, based on the current phase
def color1_code(current_phase):
    if current_phase==1:
        return 'r'
    else:
        return 'b'
def color2_code(current_phase):
    return color1_code(current_phase*-1)

# Returns the list of curve names
def get_names_for_subrange(prefix=""):
    names=["Arc1_s","Arc2_s","Force1","Force2","Velocity","AccX","AccZ"]
    for i in range(len(names)):
        names[i]=prefix+names[i]
    return names

# Returns an array of curve data (arcs,forces,velocity..) for the defined subrange of df
def get_subrange(df,from_,to_):
    list=[]
    for attr in get_names_for_subrange():
        list=list+[json.dumps(numpy.array(df[attr][from_:to_]).tolist())]
    return list
  
# List files
filenames=[]
for (dirname,_,files) in os.walk(workdir):
    for f in files:
        if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None:
            filenames=filenames+[dirname+"/"+f]
filenames.sort()
#filenames=[f for f in filenames if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None]
#filenames=[workdir+"0single/001.csv"]

sampling_time=100 #ms # Smoothing the arc curve
for f in filenames:
    df = pandas.read_csv(f,sep="\t")
    #if df["Name1"][0]!=numpy.int64(0):
    #    continue
    write_log(f,True)
    
    # sampling time in ms
    time_resolution=int((datetime.datetime.strptime(df.Time[1],"%H:%M:%S.%f")-datetime.datetime.strptime(df.Time[0],"%H:%M:%S.%f")).microseconds/1E3)
    step_size=int(round(sampling_time/time_resolution))
    write_log("Time resolution="+str(time_resolution)+"ms,step size="+str(step_size)+"data point.",True) 
    
    # Smoothing the arc curve
    f_v=step_size/2
    ar1=numpy.array(df.Arc1)
    ar1_=ar1.copy()
    for i in range(f_v,len(ar1)-f_v):
        ar1_[i]=sum(ar1[i-f_v:i+f_v+1])/(2*f_v+1)
    df["Arc1_s"]=ar1_
    ar2=numpy.array(df.Arc2)
    ar2_=ar2.copy()
    for i in range(f_v,len(ar2)-f_v):
        ar2_[i]=sum(ar2[i-f_v:i+f_v+1])/(2*f_v+1)
    df["Arc2_s"]=ar2_
    phases=[None for i in range(0,len(ar1_))]
    
    # Arc<0: catch, arc>0: release
    if ar1_[0] >=0:
        start=0
        current_phase=1 # +
        max_=-100
    else:
        start=0
        current_phase=-1 # -
        max_=100
    last_max=0
    last_max_pos=[0,0]
    max_pos=0
    
    # Check if it was a valid stroke
    def check_stroke_length(pos):
        if (max_pos-last_max_pos[0])*time_resolution>max_stroke_time_ms or (max_pos-last_max_pos[0])*time_resolution<min_stroke_time_ms:
            write_log("Invalid stroke length (ms):"+str((max_pos-last_max_pos[0])*time_resolution))
            phases[last_max_pos[0]:max_pos]=['u' for i_ in range(max_pos-last_max_pos[0])]
            plot(range(last_max_pos[0],max_pos),[-40 for i in range(last_max_pos[0],max_pos)],c="black")
    
    # Detected a change transition (when the arc reached again 0)
    def change_phase(pos):
        global current_phase
        global start
        global max_pos
        global last_max
        global last_max_pos
        global max_
        global ar1_
        global phases
        phases[start:max_pos]=[phase1_code(current_phase) for i_ in range(max_pos-start)]
        phases[max_pos:pos]=[phase2_code(current_phase) for i_ in range(pos-max_pos)]
        check_stroke_length(pos)
        plot(range(start,max_pos),ar1_[start:max_pos],c=color1_code(current_phase))
        plot(range(max_pos,pos),ar1_[max_pos:pos],c=color2_code(current_phase))
        current_phase=current_phase*(-1)
        last_max=max_
        last_max_pos[0:1]=[last_max_pos[1],max_pos]
        max_=-100*current_phase
        max_pos=pos
        start=pos
   
    # Labeling points with 'r' or 'd' (drive/recovery)
    for i in range(0,len(ar1_)):
        if sgn_(ar1_[i],last_max) != current_phase:
            change_phase(i)
        if (sgn_(ar1_[i],last_max) == 1 and ar1_[i] > max) or\
            (sgn_(ar1_[i],last_max) == -1 and ar1_[i] < max):
            max=ar1_[i]
            max_pos=i
    if start<len(ar1_)-1:
        if (len(ar1_)-last_max_pos[1])*time_resolution<max_stroke_time_ms and \
            (len(ar1_)-last_max_pos[1])*time_resolution>min_stroke_time_ms:
            change_phase(len(ar1_)-1)
        else:
            phases[last_max_pos[1]:len(ar1_)]=['u' for i_ in range(last_max_pos[1],len(ar1_)) ]
            plot(range(last_max_pos[1],len(ar1_)),ar1_[last_max_pos[1]:len(ar1_)],c=color1_code(current_phase))
            plot(range(last_max_pos[1],len(ar1_)),[-40 for i_ in range(last_max_pos[1],len(ar1_))],c="black")
   
    if plot.enabled:
        x=pandas.DataFrame([ar1,ar1_]).transpose()
        plot(x[1]*1.1,c='green')
        plot(df.Force1/10,c="yellow")
        plt.grid()
        plt.show()
  
    strokes=[]
        
    previous_phase='u'
    catch_pos=None
    basic_data=[df.MeasurementID[0],df.SessionID[0],df.Name1[0],df.Name2[0],df.OarInboard[0],df.OarLength[0],time_resolution,df.Date[0]]
    if step_size<2:
        step_size=2
    for i in range(0,len(ar1_)): 
        if phases[i]=='u':
            catch_pos=None
        if phases[i]=='d' and previous_phase=='r' and i-step_size>=0 and i+step_size<len(ar1_):
            if catch_pos != None:
                stroke=basic_data+[df["Time"][catch_pos]]+get_subrange(df,catch_pos,i)+[phases[catch_pos:i]]+catch+release
                strokes=strokes+[stroke]
                catch_pos=None
                if plot.enabled and random.random()<plot.probability:
                    plt.figure(1)
                    plt.subplot(221)
                    plt.plot(numpy.array(json.loads(catch[0])),c="r")
                    #plt.plot(numpy.array(json.loads(catch[2])),c="b")
                    #plt.plot(numpy.array(json.loads(catch[4])),c="g")
                    plt.subplot(222)
                    plt.plot(numpy.array(json.loads(release[0])),c="r")
                    #plt.plot(numpy.array(json.loads(release[2])),c="b")
                    #plt.plot(numpy.array(json.loads(release[4])),c="g")
                    plt.subplot(223)
                    plt.plot(numpy.array(json.loads(stroke[9])),c="r")
                    #plt.plot(numpy.array(json.loads(stroke[11])),c="b")
                    #plt.plot(numpy.array(json.loads(stroke[13])),c="g")
                    plt.show()
            catch=get_subrange(df,i-2*step_size,i+2*step_size)
            catch_pos=i
        if phases[i]=='r' and previous_phase=='d' and i-step_size>=0 and i+step_size<len(ar1_) and catch_pos != None :
            release=get_subrange(df,i-2*step_size,i+2*step_size)
            
        previous_phase=phases[i]
        
    df_strokes=pandas.DataFrame(strokes,columns=["MeasurementID","SessionID","Name1","Name2","OarInboard","OarLength","TimeResolutionMs","Date"]+["Time"]+
                                get_names_for_subrange()+["PhaseCodes"]+get_names_for_subrange("Catch")+
                                get_names_for_subrange("Release"))
    fname=re.search("[a-zA-Z0-9]*.[0-9]?[0-9][0-9].csv$",f).group(0)
    if df_strokes.MeasurementID.count()>0:
        df_strokes.to_csv(outdir+fname,sep="\t",header=True,index=False)
    write_log("Total strokes detected: "+str(df_strokes.MeasurementID.count()),True)

write_log.wr.close()      
