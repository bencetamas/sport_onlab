workdir="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step2/"
outdir1="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step3/"
outdir2="/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step4/"

percentile_count=10

import pandas
import numpy
import os
import re
import math

# Filter out outlier data (and correcting)
def filter_outliers(series,strategy="delta",std_multiplier=1):
    std=series.std()
    mean=series.mean()
    count=series.count()
    
    def simple_correct(x):
        if x-mean>std_multiplier*std:
            return mean+std_multiplier*std
        elif x-mean<-std_multiplier*std:
            return mean-std_multiplier*std
        else:
            return x
    
    def delta_correct(s,i):
        if i==0:
            return
        if abs(s[i]-s[i-1])>std_multiplier*std:
            #print "Found outlier at index"+str(i)+":"+str(s[i])+","+str(s[i-1])
            if (s[i]>mean and s[i]>s[i-1]):
                s[i]=s[i-1]
            elif (s[i]>mean and s[i]<=s[i-1]):
                s[i-1]=s[i]
                delta_correct(s,i-1)
            elif (s[i]<=mean and s[i]<s[i-1]):
                s[i]=s[i-1]
            else: #(s[i]<=mean and s[i]>=s[i-1]):
                s[i-1]=s[i]
                delta_correct(s,i-1)
       
    if strategy=="delta":
        s1=numpy.array(series,dtype=pandas.Series)
        for i in range(1,len(s1)): 
            #if i%1000==0:
            #    print i
            delta_correct(s1,i)
        return pandas.Series(s1)
    else:
        return series.apply(lambda x: simple_correct(x))



filenames=[]
for (dirname,_,files) in os.walk(workdir):
    for f in files:
        if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None:
        #if re.search("^030\.csv$",f) != None:
            filenames=filenames+[dirname+"/"+f]
filenames.sort()
#filenames=[f for f in filenames if re.search("^[0-9][0-9][0-9]?\.csv$",f) != None]
#filenames=[workdir+"0single/001.csv"]

# Attribute -> ,,outlier filter *std() value''
#attributes={"Force1":3.1,"Force2":3.1,"Arc1":2,"Arc2":2,"AccX":2.4,"AccZ":3.2,"Velocity":2,"StrokeRate":2}
attributes=["Force1","Force2","Arc1","Arc2","AccX","AccZ","Velocity","StrokeRate"]
#attributes={"Velocity":2}
simple_filter_attrs={"AccX":2,"AccZ":2,"StrokeRate":2}
delta_filter_attrs={"Force1":1,"Force2":1,"Arc1":1,"Arc2":1,"Velocity":1}
    
#Column names
columns=["MeasurementID","SessionID","Name1","Name2"]
for attr in attributes:
    columns=columns+[attr+"_avg",attr+"_min",attr+"_max",attr+"_std"]
    
    columns=columns+[attr+"_0_percentile_boundary"]
    for i in range(0,percentile_count):
        p_low=float(i)/percentile_count
        p_high=float(i+1)/percentile_count
        columns=columns+[attr+"_"+str(int(p_low*100))+"_"+str(int(p_high*100))+"_percentile%"]
        columns=columns+[attr+"_"+str(int(p_high*100))+"_percentile_boundary"]
    
columns=columns+["PerpendicularAvgForce"]

dropped=[]
aggr=[]
for f in filenames:
    print f
    df = pandas.read_csv(f,sep="\t")
    fr_avg=(df.Force1.mean()+df.Force2.mean())/2
    vel_avg=df.Velocity.mean()
    if vel_avg < 1.5 or fr_avg < 32:
        print "Dropping file "+f+" as an outlier. (vel.:"+str(vel_avg)+"m/s, force:"+str(fr_avg)+"N)"
        dropped=dropped+[[f,df.MeasurementID[0],df.SessionID[0],df.Name1[0],df.Name2[0],vel_avg,fr_avg]]
        continue
    
    df["StrokeRate"]=60/(df["WebaDStrokeTime(1)"]/1E3)
    
    aggr_row=[df.loc[0].MeasurementID,df.loc[0].SessionID,df.loc[0].Name1,df.loc[0].Name2]
    for attr in attributes:
        #print attr
        min=df[attr].min()
        max=df[attr].max()
        std=df[attr].std()
        count=df[attr].count()
        mean=df[attr].mean()
        aggr_row=aggr_row+[mean,min,max,std]  
        
        
        s=df[attr].copy(deep=True)
        if s.count() > 0:
            if attr in simple_filter_attrs.keys():
                s=filter_outliers(s,strategy="simple",std_multiplier=simple_filter_attrs[attr])
            if attr in delta_filter_attrs.keys():
                s=filter_outliers(s,strategy="delta",std_multiplier=delta_filter_attrs[attr])
            df[attr]=s
            
            min_f=s.min()
            max_f=s.max()
            aggr_row=aggr_row+[min_f]    
            for i in range(0,percentile_count):
                p_low=min_f+(max_f-min_f)*float(i)/percentile_count
                p_high=min_f+(max_f-min_f)*float(i+1)/percentile_count
                aggr_row=aggr_row+[df.query("("+attr+">="+str(p_low)+") and ("+attr+"<"+str(p_high)+")")[attr].count()/float(count)*100]
                aggr_row=aggr_row+[p_high]
        else:
            min_f=numpy.core.numeric.NaN
            max_f=numpy.core.numeric.NaN
            aggr_row=aggr_row+[numpy.core.numeric.NaN]    
            for i in range(0,percentile_count):
                aggr_row=aggr_row+[numpy.core.numeric.NaN]
                aggr_row=aggr_row+[numpy.core.numeric.NaN]
        
    fname=re.search("[0-9][0-9][0-9]?\.csv$",f).group(0)
    if re.search("single/[0-9][0-9][0-9]?\.csv$",f) != None:
        fname="0single/"+fname
    elif re.search("pair/[0-9][0-9][0-9]?\.csv$",f) != None:
        fname="1pair/"+fname
    df.to_csv(outdir2+fname,sep="\t",header=True)
    
    df["PerpendicularForceAvg"]=df.apply(lambda x: (x.Force1*math.cos(x.Arc1/180.0*2*3.14)+x.Force2*math.cos(x.Arc2/180.0*2*3.14))/2,axis=1)
    aggr_row=aggr_row+[df["PerpendicularForceAvg"].mean()]
    aggr=aggr+[aggr_row]

aggr_df=pandas.DataFrame(aggr,columns=columns)
aggr_df.to_csv(outdir1+"aggregate.csv",header=True,sep="\t",na_rep="NaN")
aggr_df.to_csv(outdir1+"aggregate_not_all.csv",header=True,sep="\t",na_rep="NaN",columns=["MeasurementID","SessionID","Name1","Name2","Velocity_avg","Velocity_std"
                                                                                         ,"Velocity_max","Force1_avg","Force1_std","Force1_max","Force2_avg","Force2_std"
                                                                                         ,"Force2_max","Arc1_min","Arc1_max",
                                                                                         "Arc1_avg","Arc1_srd","Arc2_min","Arc2_max","Arc2_avg","Arc2_std"])
dropped_df=pandas.DataFrame(dropped,columns=["File","MeasurementID","SessionID","Name1","Name2","Velocity_avg","Force_avg"])
dropped_df.to_csv(outdir2+"dropped_csv.log",sep="\t",header=True)
