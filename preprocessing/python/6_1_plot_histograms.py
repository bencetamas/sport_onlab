import pandas as pd
import numpy
import matplotlib.pyplot as plt

df=pd.read_csv("/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step3/aggregate.csv",sep="\t")

row=6
attributes=["Velocity"]
axis=[]
data=[]
percentile_count=10
for attr in attributes:
    for i in range(0,percentile_count):
        p_low=float(i)/percentile_count
        p_high=float(i+1)/percentile_count
        axis=axis+[(df[attr+"_"+str(int(p_low*100))+"_percentile_boundary"][row]+
                   df[attr+"_"+str(int(p_high*100))+"_percentile_boundary"][row])/2]
        data=data+[df[attr+"_"+str(int(p_low*100))+"_"+str(int(p_high*100))+"_percentile%"][row]]
 
print axis
print data
plt.figure(1)
plt.subplot(221)
#plt.axis([0,600,0,80])
#plt.axis([-60,60,0,20])
plt.axis([0,10,0,40])
plt.plot(axis,data)
plt.subplot(222)
plt.bar(axis,data,width=14,color='r')
#plt.axis([0,600,0,80])
#plt.axis([-60,60,0,20])
plt.axis([0,10,0,40])
plt.show()
