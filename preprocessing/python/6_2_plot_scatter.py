import pandas as pd
import numpy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

df=pd.read_csv("/home/tamasbence/Documents/egyetem/Msc/weba_temp/weba_step3/aggregate.csv",sep="\t")

fr_avg=numpy.array((df.Force1_avg+df.Force2_avg)/2)
fr_min=numpy.array(df.Force1_0_percentile_boundary)
fr_max=numpy.array(df.Force1_100_percentile_boundary)

vel_avg=numpy.array(df.Velocity_avg)
vel_std=numpy.array(df.Velocity_std)
vel_max=numpy.array(df.Velocity_100_percentile_boundary)
    
plt.figure(1)
ax=plt.subplot(221)
ax.set_xlabel("Avg. velocity (m/s)")
ax.set_ylabel("Avg. force (N)")
plt.scatter(vel_avg[0:40],fr_avg[0:40],s=vel_std*20,color='b')
plt.scatter(vel_avg[41:],fr_avg[41:],s=vel_std*20,color='r')
plt.axis([0,10,0,220])
ax.yaxis.set_ticks(numpy.arange(0, 220, 15))
ax.xaxis.set_ticks(numpy.arange(0, 10, 0.5))
plt.grid()
ax=plt.subplot(222)
ax.set_xlabel("Avg. velocity (m/s)")
ax.set_ylabel("Max force (N)")
plt.axis([0,10,0,900])
plt.scatter(vel_avg,fr_max,s=vel_std*40)
ax=plt.subplot(223)
ax.set_xlabel("Max velocity (m/s)")
ax.set_ylabel("Max force (N)")
plt.axis([0,15,0,800])
plt.scatter(vel_max,fr_max,s=vel_std*20)
plt.show()
